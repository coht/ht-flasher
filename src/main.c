
/*
 * Copyright (C) 2013-2018 CERN (www.cern.ch)
 * Author: Tomasz Włostowski <tomasz.wlostowski@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <unistd.h>
#include <sys/time.h>

#include "ht_flasher.h"
#include "xbf.h"

struct arglist_t *args_parse(int argc, char *argv[])
{
    struct arglist_t *args = malloc(sizeof(struct arglist_t));
    int i = 1;
    int expect_value = 0;
    int optindex = 0;

    args->options = malloc((argc + 1) * sizeof(struct arglist_option_t));

    while (i < argc)
    {
        char *p = argv[i];

        //printf("i %d optindex %d\n", i, optindex );
        assert(p);

        if( strlen(p) && p[0] == '-' && expect_value)
        {
            optindex++;
            args->options[optindex].option = strdup(p+1);
            args->options[optindex].value_str = NULL;
            expect_value = 1;
        }

        if (strlen(p) && p[0] != '-' && !expect_value)
        {
            //  printf("free %s i %d\n", p, i);
            expect_value = 0;
            args->options[optindex].option = strdup("");
            args->options[optindex].value_str = strdup(p);
            optindex++;
        }

        if (expect_value)
        {
            args->options[optindex].value_str = strdup(p);
            //printf("opt %s val %s\n", args->options[optindex].option, args->options[optindex].value_str );
            //printf("expect val %s i %d\n", p, i);
            optindex++;
            expect_value = 0;
        }

        if (strlen(p) >= 2 && p[0] == '-')
        {
            expect_value = 1;
            args->options[optindex].option = strdup(p + 1);
            args->options[optindex].value_str = NULL;
        }

        i++;
    }

    if(expect_value)
        optindex++;

    args->n_options = optindex;

    return args;
}

const char *arg_get_value_str(struct arglist_t *args, const char *opt, const char *default_value)
{
    int i;
    for (i = 0; i < args->n_options; i++)
    {
        if (!strcmp(opt, args->options[i].option))
        {

            return args->options[i].value_str;
        }
    }

    return default_value;
}

int arg_is_present(struct arglist_t *args, const char *opt)
{
    int i;
    for (i = 0; i < args->n_options; i++)
    {
        if (!strcmp(opt, args->options[i].option))
        {
            return 1;
        }
    }

    return 0;
}

void flasher_error(const char *fmt, ...)
{
    va_list vargs;
    va_start(vargs, fmt);
    vfprintf(stderr, fmt, vargs);
    va_end(vargs);
    fflush(stdout);
}

void flasher_report(const char *fmt, ...)
{
    va_list vargs;
    va_start(vargs, fmt);
    vfprintf(stdout, fmt, vargs);
    va_end(vargs);
    fflush(stdout);
}

static uint64_t get_tics(void)
{
    struct timezone tz = {0, 0};
    struct timeval tv;
    gettimeofday(&tv, &tz);
    return (uint64_t)tv.tv_sec * 1000000ULL + tv.tv_usec;
}

void udelay(uint32_t microseconds)
{
    uint64_t t_start = get_tics();

    while (t_start + microseconds >= get_tics())
        ;
}

struct binary_buffer_t *read_binary_file(const char *filename)
{
    FILE *f = fopen(filename, "rb");
    if (!f)
        return NULL;

    struct binary_buffer_t *buf = malloc(sizeof(struct binary_buffer_t));

    fseek(f, 0, SEEK_END);
    buf->size = ftell(f);
    fseek(f, 0, SEEK_SET);

    buf->file_name = strdup(filename);
    buf->data = malloc(buf->size);
    if (fread(buf->data, 1, buf->size, f) != buf->size) {
      fclose (f);
      free (buf);
      return NULL;
    }

    buf->part_name = strdup("Unknown FPGA/MCU");
    buf->date = strdup("Unknown");
    buf->time = strdup("Unknown");

    fclose(f);
    return buf;
}

struct binary_buffer_t *read_xilinx_bitfile(const char *filename)
{
    struct xbf xbf;
    struct binary_buffer_t *buf = malloc(sizeof(struct binary_buffer_t));

    xbf_init(&xbf);

    if (xbf_open(&xbf, filename) != 0)
    {
        fprintf(stderr, "Error loading '%s': %s\n", filename, xbf_errmsg(&xbf));
        return NULL;
    }

    buf->file_name = strdup(filename);
    buf->data = (uint8_t *)xbf.xbf_data;
    buf->size = xbf.xbf_len;

    buf->part_name = strdup(xbf.xbf_partname);
    buf->date = strdup(xbf.xbf_date);
    buf->time = strdup(xbf.xbf_time);

    return buf;
}

struct binary_buffer_t *read_altera_rpdfile(const char *filename)
{
    FILE *f = fopen(filename, "rb");
    if (!f)
        return NULL;

    struct binary_buffer_t *buf = malloc(sizeof(struct binary_buffer_t));

    fseek(f, 0, SEEK_END);
    buf->size = ftell(f);
    fseek(f, 0, SEEK_SET);

    buf->file_name = strdup(filename);
    buf->data = malloc(buf->size);

    // Raw Programming Data File (.rpd) Definition from Altera:
    // Data written to a serial configuration device should be shifted so
    // that the least-sgnificant bit is loaded into the device first.
    // For example, if the .rpd contains the byte sequence 02 1B EE 01 FA,
    // the serial data programmed into the configuration device must
    // be 100-0000 1101-10000111-0111 1000-0000 0101-1111.

    // Reading binary contents and swapping byte by byte
    uint8_t tmp;
    for (uint32_t i=0; i<buf->size; i++) {
        if (fread(&tmp, sizeof(uint8_t), 1, f) != 1) {
            fclose (f);
            free (buf);
            return NULL;
        }
        buf->data[i] = reverse_bits8(tmp);
    }

    buf->part_name = strdup("Unknown FPGA/MCU");
    buf->date = strdup("Unknown");
    buf->time = strdup("Unknown");

    fclose(f);
    return buf;
}

void print_bitfile_info(struct binary_buffer_t *buf)
{
    printf("Image: %s (%ld bytes), part: %s, date: %s %s\n", buf->file_name, buf->size, buf->part_name, buf->date, buf->time);
}


int write_binary_file(const char *filename, struct binary_buffer_t *buf)
{
    FILE *f=fopen(filename,"wb");
    if(!f) {
        flasher_error("cannot open %s: %m\n", filename);
        return -1;
    }

    fwrite(buf->data,1,buf->size,f);
    fclose(f);
    return 0;
}


struct binary_buffer_t *read_bitfile(const char *filename)
{
    if (strlen(filename) > 4 && !strcmp(filename + strlen(filename) - 4, ".bin"))
    {
        return read_binary_file(filename);
    }
    else if (strlen(filename) > 4 && !strcmp(filename + strlen(filename) - 4, ".bit"))
    {
        return read_xilinx_bitfile(filename);
    }
    else if (strlen(filename) > 4 && !strcmp(filename + strlen(filename) - 4, ".rpd"))
    {
        return read_altera_rpdfile(filename);
    }

    fprintf(stderr, "Unknown bitstream file type: %s\n", filename);
    return NULL;
}

static struct algo_t *algos[] =
{
        &algo_spec_fpga,
        &algo_spec_flash,
        &algo_spec_eeprom,
        &algo_sis83k_flash,
        &algo_afcz_flash,
        &algo_ctra_flash,
        &algo_svec_app_flash,
        &algo_svec_boot_flash,
        &algo_svec_app_direct,
        &algo_generic_vme,
        &algo_wr2rf_flash,
	    &algo_zynqus_flash,
	    &algo_wren_vme_flash,
	    &algo_mena25,
	    &algo_eda_02175,
        NULL
};

static struct algo_t *find_algo(const char *name)
{
    int i;
    for (i = 0; algos[i]; i++)
        if (!strcasecmp(name, algos[i]->name))
                return algos[i];
    return NULL;
}

void list_algos(void)
{
    int i;
    printf("List of supported flashing targets/algorithms: \n");
    for (i = 0; algos[i]; i++)
    {
        printf(" %-16s: %s\n", algos[i]->name, algos[i]->description );
    }
}

void help(const char *algo_name)
{
    if (algo_name)
    {
        struct algo_t *algo = find_algo(algo_name);

        if (!algo)
        {
            flasher_error("Unknown flashing algorithm: '%s'\n", algo_name);
            return;
        }

        algo->help(algo);
        return;
    }

    printf("ht-flasher: a swiss-army knife for flashing BE-CO-HT products.\n");
    printf("\nOptions: \n");
    printf(" -a algorithm   : select flashing algorithm/target.\n");
    printf(" -d device      : device ID to be flashed.\n");
    printf(" -l             : list all supported algorithms/targets.\n");
    printf(" -h [algorithm] : display this message or the help for the particular flashing algorithm.\n");
    printf(" -w filename    : write the given file to flash.\n");
    printf(" -r filename    : read back the flash to the file provided.\n");
    printf(" -e             : bulk erase the flash.\n");
    printf("\n");
    list_algos();
}

int main(int argc, char *argv[])
{
    struct arglist_t *args = args_parse(argc, argv);
    const char *algo_name = arg_get_value_str(args, "a", NULL);

    if( arg_is_present( args, "l" ) )
    {
        list_algos();
        return 0;
    }

    if( arg_is_present( args, "h" ) )
    {
        const char *algo_name = arg_get_value_str(args, "h", NULL);

        help( algo_name );
        return 0;
    }


    if (!algo_name)
    {
        flasher_error("Expected flashing algorithm name. Use '-h' option for help or '-l' option to list all supported algorithms.\n");
        return -1;
    }

    struct algo_t *algo = find_algo(algo_name);

    if (!algo)
    {
        flasher_error("Unknown flashing algorithm: '%s'\n", algo_name);
        return -1;
    }

    if (algo->init(algo, args) < 0)
    {
        flasher_error("Flashing algorithm initialization failed.\n");
        return -1;
    }

    if (algo->run(algo))
    {
        flasher_error("Flashing error!\n");
        return -1;
    }

    algo->release(algo);

    return 0;
}
