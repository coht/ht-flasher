/*
 * Copyright (C) 2013-2018 CERN (www.cern.ch)
 *
 * Released according to the GNU GPL, version 2 or any later version.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/errno.h>
#include <assert.h>

#include <ual.h>

#include "ht_flasher.h"
#include "flash.h"

#define swap32(x) __builtin_bswap32(x)

#define FLASH_CONTROL 0x200
#define FLASH_DATA    0x204
#define FLASH_BOARD   0x208
#define FLASH_BOOT    0x20c

struct mena25_bus_t {
        struct ual_bar_tkn* bar;
	/* chip select (1, 2 or 3) */
	unsigned cs;
};

#if 0
static uint32_t sys_readl(struct mena25_bus_t *bus, uint32_t addr)
{
        uint32_t d = ual_readl(bus->bar,  addr );
        return d;
}

static void sys_writel(struct mena25_bus_t *bus, uint32_t data, uint32_t addr)
{
        ual_writel(bus->bar, addr, data );
}

static void sys_write_cmd(struct mena25_bus_t *bus, uint32_t data)
{
        //printf("GEN_FIFO<- %08x\n", data);
        ual_writel(bus->bar, GQSPI_GEN_FIFO, data);
}
#endif

static struct mena25_bus_t *bus;

static int mena25_pci_open(struct arglist_t* args )
{
        const char *device = arg_get_value_str(args, "d", NULL);

        if (!device) {
                flasher_error( "Expected PCI bus/device ID.\n" );
                return -1;
        }

        struct ual_desc_pci desc;

	memset( &desc, 0, sizeof( struct ual_desc_pci ));

	int b, d;
	int i = sscanf(device, "%x:%x", &b, &d);
	if (i == 2) {
		b &= 0xFF;
		d &= 0xFF;
		desc.devid = (b << 16) | (d << 8);
	}
	else {
		flasher_error("Bus/device ID must be in bus:dev format.\n");
		return -1;
	}

        desc.data_width = 4;
	desc.bar = 0; /**< PCI Base Address Register to access */
	desc.size = 4096; /**< number of bytes to maps (PAGE_SIZE aligned) */
	desc.offset = 0;
        desc.flags = 0;


	struct ual_bar_tkn *ual = ual_open( UAL_BUS_PCI,  &desc);

        if (!ual) {
                flasher_error("Can't initialize UAL: %s\n",
                              ual_strerror(errno));
                return -1;
        }

        bus = malloc(sizeof(struct mena25_bus_t));
        bus->bar = ual;

#if 0
	mena25_qspi_init(bus);
#endif
        return 0;
}

static unsigned mena25_flash_read_status (void)
{
	ual_writel(bus->bar, FLASH_CONTROL, 1 << 27);
	return ual_readl(bus->bar, FLASH_DATA);
}

static int mena25_flash_erase_sector(struct spi_flash_chip_t *chip,
				     uint32_t addr)
{
	/* Erase command */
	ual_writel(bus->bar, FLASH_CONTROL, (1 << 28) | addr);
	ual_writel(bus->bar, FLASH_DATA, 0);

	while (1) {
		unsigned status = ual_readl(bus->bar, FLASH_CONTROL);
		if (status & (1 << 30))
			continue;
		if (status & (1 << 29)) {
			printf ("erase failed (status=%08x)\n", status);
			return -1;
		}
		else
			return 0;
	}
}

static uint32_t readw (const uint8_t *buf, unsigned size, unsigned addr)
{
	unsigned w = 0;
	for (unsigned j = addr; j < addr + 4; j++) {
		uint8_t b = (j < size) ? buf[j] : 0xff;
		w |= b << (8 * j);
	}
	return w;
}

static void mena25_flash_program_page(struct spi_flash_chip_t *chip,
				       uint32_t addr,
				       const uint8_t *data, int size)
{
	unsigned page_size = chip->page_size;
	unsigned i;

	assert (size <= page_size);

	for (i = 0; i < page_size; i += 4) {
		uint32_t w = readw (data, size, i);
		ual_writel(bus->bar, FLASH_DATA, w);
	}

	ual_writel(bus->bar, FLASH_CONTROL, (1 << 30) | addr);

	while (1) {
		unsigned status = ual_readl(bus->bar, FLASH_CONTROL);
		if (!(status & (1 << 30)))
			break;
	}
}

static unsigned mena25_flash_readw (unsigned addr)
{
	ual_writel(bus->bar, FLASH_CONTROL, addr);
	return ual_readl(bus->bar, FLASH_DATA);
}

static int mena25_flash_program(struct spi_flash_chip_t *chip,
				struct mena25_bus_t *bus,
				uint32_t addr, const uint8_t *data, int size)
{
	int n = 0;
	unsigned char sector_map[chip->size / chip->sector_size];
	uint32_t remaining = size;
	unsigned page_size = chip->page_size;
	unsigned nerr;

	memset(sector_map, 0, sizeof(sector_map));

        /* Progam page by page. */
	while (n < size)
	{
		int plen = (remaining > page_size ? page_size : remaining);
		unsigned ad = (addr + n);
		int sector = (ad / chip->sector_size);

		if (!sector_map[sector])
		{
			flasher_report("Erasing sector 0x%x            \r", ad);
			mena25_flash_erase_sector(chip, ad);
			sector_map[sector] = 1;
		}

		if( (n % 20) == 0 )
			flasher_report("Programming page %d/%d.        \r",
				       n / chip->page_size,
				       (size + chip->page_size - 1) / chip->page_size - 1);

		mena25_flash_program_page(chip, ad, data + n, plen);

		remaining -= plen;
		n += plen;
	}


	nerr = 0;

	for (n = 0; n < size; n += 4)
	{
		uint32_t w = readw (data, size, n);
		uint32_t r = mena25_flash_readw(addr + n);

		if( (n % 10000) == 0 )
		{
			flasher_report("Verification: %.0f%%        \r",
				       (double)n / (double) size * 100.0);
		}

		if (r != w)
		{
			flasher_report(
					"Verification failed at offset 0x%06x (is: 0x%08x, should be: 0x%08x)\n.",
					addr + n, r, w);
			if (nerr++ == 10)
				return -1;
		}
	}

	flasher_report("Verification: %.0f%%          \n", 100.0);

	return nerr ? -1 : 0;
}


static const char *filename;
static int do_table, do_read, do_write, do_dump, do_fpga_header;
static int do_info, do_boot;
static unsigned address = ~0U;
static unsigned length;

static int mena25_flash_init( struct algo_t *self, struct arglist_t *args )
{
	do_table = arg_is_present(args, "t");
	do_read = arg_is_present(args, "r");
	do_write = arg_is_present(args, "w");
	do_dump = arg_is_present(args, "D");
	do_fpga_header = arg_is_present(args, "H");
	do_info = arg_is_present(args, "i");
	do_boot = arg_is_present(args, "b");

	if (do_table + do_read + do_write
	    + do_dump + do_fpga_header
	    + do_info + do_boot != 1) {
		flasher_error("need one action option (-i/-w/-t/-D/-H/-b)\n");
		return -1;
	}

	if (do_read)
		filename = (char *)arg_get_value_str(args, "r", NULL);

	if (do_write)
		filename = (char *)arg_get_value_str(args, "w", NULL);

	if (arg_is_present(args, "o")) {
		char *arg = (char *)arg_get_value_str(args, "o", NULL);
		char *e;
		address = strtoul(arg, &e, 0);
		if (*e) {
			flasher_error("incorrect option -o\n");
			return -1;
		}
	}

	if (arg_is_present(args, "s")) {
		char *arg = (char *)arg_get_value_str(args, "s", NULL);
		char *e;
		length = strtoul(arg, &e, 0);
		if (*e) {
			flasher_error("incorrect option -s\n");
			return -1;
		}
	}

	if (mena25_pci_open(args) < 0)
		return -1;

	return 0;
}

static void mena25_flash_help(struct algo_t *self)
{
	flasher_report("Programs the FPGA Flash of the VME bridge\n\n");
	flasher_report("Usage: ht-flasher -a men_a25 -d <device_id> command\n");
	flasher_report("The device ID is in bus:dev_fn format (as in lspci). For example, if the device is on PCI bus 0xa at dev/function 0, the string should be '-d 0a:00'.\n\n");
	flasher_report("ht-flasher -a mena25 -d <device_id> -t\n");
	flasher_report("   display chameleon table\n\n");
	flasher_report("ht-flasher -a mena25 -d <device_id> [-o <addr> -s <size>] -D\n");
	flasher_report("   dump flash content\n\n");
	flasher_report("ht-flasher -a mena25 -d <device_id> [-o <addr>] -H\n");
	flasher_report("   dump bitstream header\n\n");
	flasher_report("ht-flasher -a mena25 -d <device_id> -i\n");
	flasher_report("   disp boot info\n\n");
	flasher_report("ht-flasher -a mena25 -d <device_id> -w <filename.bin>\n");
	flasher_report("   write application bitstream\n\n");
	flasher_report("ht-flasher -a mena25 -d <device_id> [-o <addr>] -b\n");
	flasher_report("   reconfigure fpga using bitstream at <addr>\n");
}

static void print_padded_string(const unsigned *w, unsigned len)
{
	for (unsigned i = 0; i < len; i++) {
		for (unsigned k = 0; k < 4; k++) {
			unsigned c = (w[i] >> (8*k)) & 0xff;
			if (c == 0)
				c = ' ';
			printf ("%c", c >= 32 && c < 128 ? c : '.');
		}
	}
}

static void mena25_disp_table(void)
{
	unsigned desc[5];
	unsigned off;

	/* Raw dump */
	for (unsigned i = 0; i < 256; i+= 16) {
		printf("%02x: ", i);
		for (unsigned j = i; j < i + 16; j += 4)
			printf (" %08x", ual_readl(bus->bar, j));
		printf ("  ");
		for (unsigned j = i; j < i + 16; j += 4) {
			unsigned v = ual_readl(bus->bar, j);
			for (unsigned k = 0; k < 4; k++) {
				unsigned c = (v >> (8*k)) & 0xff;
				printf ("%c", c >= 32 && c < 128 ? c : '.');
			}
		}
		printf("\n");
	}

	/* Read header */
	{
		for (unsigned i = 0; i < 5; i++)
			desc[i] = ual_readl(bus->bar, i * 4);
		unsigned model = (desc[0] >> 8) & 0xff;
		printf ("Model: %02x (%c), ", model, model);
		printf ("rev: %u.%u, ", desc[0] & 0xff, (desc[0] >> 16) & 0xff);
		printf ("magic: 0x%04x", desc[1] & 0xffff);
		printf (", file: '");
		print_padded_string(desc + 2, 4);
		printf ("'\n");
	}
	off = 5 * 4;

	/* Cf Descriptor.pm */
	for (unsigned idx = 0; idx < 20; idx++) {
		unsigned typ;
		desc[0] = ual_readl(bus->bar, off);
		typ = desc[0] >> 28;
		if (typ == 0x0f)
			break;
		else if (typ == 0x00) {
			printf ("%02u dev: %03x, ",
				idx, (desc[0] >> 18) & 0x3ff);
			desc[1] = ual_readl(bus->bar, off + 4);
			printf ("bar: %u, ", desc[1] & 0x7);
			desc[2] = ual_readl(bus->bar, off + 8);
			printf ("off: %08x, ", desc[2]);
			desc[3] = ual_readl(bus->bar, off + 12);
			printf ("size: %08x\n", desc[3]);
			off += 4 * 4;
		}
		else {
			printf ("unknown typ (%x)\n", typ);
			break;
		}
	}
}

static void mena25_dump(void)
{
	if (length == 0)
		length = 0x100;
	if (address == ~0U)
		address = 0;

	for (unsigned i = 0; i < length; i += 16) {
		printf ("%08x: ", address + i);
		for (unsigned j = i; j < i + 16 && j < length; j += 4) {
			printf (" %08x", mena25_flash_readw(address + j));
		}
		printf ("\n");
	}
				
}

/* From 16t036-00_src fpga_header.h */
#define FPGA_HEADER_MAGIC 0x51d4268e	 /**< Magic number for original header */
#define FPGA_LONGHEADER_MAGIC 0x52d4268e /**< Magic number for long header */

#define FPGA_SIZE_HEADER_SHORT 0x030	/**< length of original FPGA header */
#define FPGA_SIZE_HEADER_LONG  0x100    /**< length of long FPGA header */

static void mena25_dump_fpga_header(void)
{
	unsigned magic;

	if (address == ~0U)
		address = 0x200000;

	magic = mena25_flash_readw(address + 0);

	printf ("magic at 0x%08x: %08x\n", address, magic);

	if (magic == FPGA_LONGHEADER_MAGIC) {
		unsigned name[8];
		unsigned date[2];

		for (unsigned i = 0; i < 7; i++)
			name[i] = mena25_flash_readw(address + 4 + i * 4);
		printf ("filename: '");
		print_padded_string(name, 7);

		for (unsigned i = 0; i < 4; i++)
			name[i] = mena25_flash_readw(address + 32 + i * 4);
		printf ("', fpga: '");
		print_padded_string(name, 4);
		printf ("'\n");
		printf ("size: %u\n",
			swap32 (mena25_flash_readw(address + 48)));
		date[0] = swap32 (mena25_flash_readw(address + 56));
		date[1] = swap32 (mena25_flash_readw(address + 60));
		printf ("date: %u-%02u-%02u %02u:%02u\n",
			date[0] >> 16, (date[0] >> 8) & 0xff, date[0] & 0xff,
			date[1] >> 24, (date[1] >> 16) & 0xff);
		for (unsigned i = 0; i < 4; i++)
			name[i] = mena25_flash_readw(address + 64 + i * 4);
		printf ("board type: '");
		print_padded_string(name, 4);
		printf ("'\n");
	}
	else
		printf("unhandled magic: 0x%08x\n", magic);
}

static void mena25_boot_info(void)
{
	unsigned status;
	printf ("control:      %08x\n", ual_readl(bus->bar, FLASH_CONTROL));
	printf ("data:         %08x\n", ual_readl(bus->bar, FLASH_DATA));
	status = ual_readl(bus->bar, FLASH_BOARD);
	printf ("board status: %08x\n", status);
	printf (" image: ");
	switch (status & 3) {
	case 0x00:
		printf ("fallback\n");
		break;
	case 0x01:
		printf ("application\n");
		break;
	case 0x02:
		printf ("fallback after error\n");
		break;
	default:
		printf ("invalid\n");
		break;
	}
	printf ("boot-addr:    %08x\n", ual_readl(bus->bar, FLASH_BOOT));
	ual_writel(bus->bar, FLASH_CONTROL, 1 << 25);
	printf ("flash id:     %08x (15=M25P32 - 4MB)\n",
		ual_readl(bus->bar, FLASH_DATA));
}

static void mena25_boot(void)
{
	if (address == ~0U)
		ual_writel(bus->bar, FLASH_CONTROL, 1 << 31);
	else
		ual_writel(bus->bar, FLASH_BOOT, address);
}

static int mena25_flash_run(struct algo_t *self)
{
	if (do_table) {
		mena25_disp_table();
		return 0;
	}
	if (do_dump) {
		mena25_dump();
		return 0;
	}
	if (do_fpga_header) {
		mena25_dump_fpga_header();
		return 0;
	}
	if (do_info) {
		mena25_boot_info();
		return 0;
	}
	if (do_boot) {
		mena25_boot();
		return 0;
	}

	if (do_write) {
		ual_writel(bus->bar, FLASH_CONTROL, 1 << 25);
		unsigned sid = ual_readl(bus->bar, FLASH_DATA);
		struct spi_flash_chip_t* chip;

		if (sid == 0x15)
			chip = &spi_flash_chip_m25p32;
		else {
			printf ("unknown flash silicon id: %08x\n", sid);
			return 1;
		}
		struct binary_buffer_t *data = read_binary_file( filename );

		if (!data) {
			flasher_error("Can't open binary file '%s'\n",
                                      filename);
			return -1;
		}

		if (readw (data->data, data->size, 0) != FPGA_LONGHEADER_MAGIC) {
			printf(".bin file required (with fpga header)\n");
			return -1;
		}

		if (address == ~0U)
			address = 0x200000;

		return mena25_flash_program(chip, bus, address,
					    data->data, data->size);
	}
#if 0
	if (do_read) {
		struct binary_buffer_t data;
		unsigned off;
		int res;
		data.size = length ? length : 0x20000;
		data.data = malloc(data.size);

		bus->cs = 3;

		for (off = 0; off < data.size; ) {
			res = mena25_qspi_read_data(bus, address + off,
						    data.size - off,
						    data.data + off);
			if (res < 0)
				return -1;
			off += res;
		}
		write_binary_file(filename, &data);
		return 0;
	}
	else {
		/* Read Data */

		uint8_t data[0x50];

		bus->cs = 3;

		mena25_qspi_read_data(bus, address, sizeof (data), data);
		for (unsigned i = 0; i < sizeof (data); i++) {
			if ((i % 16) == 0)
				printf("data[%02d]:", i);
			printf(" %02x", data[i]);
			if ((i % 16) == 15 || i == sizeof(data) - 1)
				printf("\n");
		}
	}
#if 0
	struct spi_flash_chip_t *candidates[] = {
		&spi_flash_chip_s25fl128, &spi_flash_chip_mx25l25635f,
		&spi_flash_chip_n25q256, &spi_flash_chip_m25qu512
	};
	struct spi_flash_chip_t *flash = NULL;
	int i;
	uint32_t id = 0;


	for (i = 0; i < sizeof(candidates) / sizeof(candidates[0]); ++i) {
		candidates[i]->bus = mena25_spi;

		id = spi_flash_read_id(candidates[i]);

		if (id == candidates[i]->id) {
			flash = candidates[i];
			break;
		}
	}

	if (!flash) {
		flasher_error( "Unknown flash type: %x\n", id );
		return -1;
	}

	flasher_report( "Detected flash type: %s\n", flash->name );

	if (do_write) {
		struct binary_buffer_t *data = read_bitfile( filename );

		if (!data) {
			flasher_error("Can't open bitstream file '%s'\n",
                                      filename);
			return -1;
		}

		printf("\n");
		print_bitfile_info( data );

		return spi_flash_program(flash, 0, data->data, data->size);
	} else if (do_read) {
		struct binary_buffer_t data;
		data.size = 0x20000; // fixme flash.size;
		data.data = malloc(data.size);
		int rv = spi_flash_readback(flash, 0, data.data, data.size);
		(void ) rv;
		write_binary_file(filename, &data);
		return 0;
	}
#endif
#endif
	return 0;
}

static int mena25_flash_release(struct algo_t *self)
{
        return 0;
}

struct algo_t algo_mena25 = {
	"mena25",
	"MEN A25 VME-bridge FPGA flash",
	mena25_flash_init,
	mena25_flash_help,
	mena25_flash_run,
	mena25_flash_release
};
