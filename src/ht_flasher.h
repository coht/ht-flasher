
/*
 * Copyright (C) 2013-2018 CERN (www.cern.ch)
 * Author: Tomasz Włostowski <tomasz.wlostowski@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 *
 */

#ifndef __HT_FLASHER_H
#define __HT_FLASHER_H

#include <stdio.h>
#include <stdint.h>

struct binary_buffer_t {
    char *part_name;
    char *file_name;
    char *date;
    char *time;
    uint8_t *data;
    uint64_t size;
};

struct arglist_option_t
    {
        char *option;
        char *value_str;
        uint64_t value_int;
    };

struct arglist_t
{
    struct arglist_option_t *options;
    int n_options;
};

struct spi_bus_t
{
    int (*init)(struct spi_bus_t *, struct arglist_t* args);
    int (*release)(struct spi_bus_t *);
    void (*set_cs)(struct spi_bus_t *, int);
    uint8_t (*read8)(struct spi_bus_t *);
    void (*write8)(struct spi_bus_t *, uint8_t);
    void (*delay_us)(struct spi_bus_t *, int);
    void *priv;
};

struct i2c_bus_t
{
    int (*init)(struct i2c_bus_t *, struct arglist_t* args);
    void (*release)(struct i2c_bus_t *);
    int (*read)(struct i2c_bus_t *, uint8_t addr, uint8_t* data, int size);
    int (*write)(struct i2c_bus_t *, uint8_t addr, uint8_t* data, int size);

    void *priv;
};

struct i2c_bitbang_t {
    void (*set_scl)(struct i2c_bitbang_t *bus, int value );
    void (*set_sda)(struct i2c_bitbang_t *bus, int value );
    int (*get_sda)(struct i2c_bitbang_t *bus);
    void (*bit_delay)(struct i2c_bitbang_t *bus);
    void *priv;
};

struct algo_t
{
    char *name;
    char *description;

    int (*init)(struct algo_t *self, struct arglist_t *args);
    void (*help)(struct algo_t *self);
    int (*run)(struct algo_t *self);
    int (*release)(struct algo_t *self);
};


struct spi_flash_chip_t
{
	char *name;
	uint64_t id;
	uint64_t size;
	uint64_t sector_size;
	uint64_t page_size;
	struct spi_bus_t *bus;
};

extern struct spi_flash_chip_t spi_flash_chip_m25p32;
extern struct spi_flash_chip_t spi_flash_chip_m25p128;
extern struct spi_flash_chip_t spi_flash_chip_s25fl128;
extern struct spi_flash_chip_t spi_flash_chip_s25fl128l;
extern struct spi_flash_chip_t spi_flash_chip_mx25l25635f;
extern struct spi_flash_chip_t spi_flash_chip_n25q256;
extern struct spi_flash_chip_t spi_flash_chip_n25q128;
extern struct spi_flash_chip_t spi_flash_chip_m25qu512;
extern struct spi_flash_chip_t spi_flash_chip_w25q64;
extern struct spi_flash_chip_t spi_flash_chip_epcs64;

extern struct algo_t algo_spec_fpga;
extern struct algo_t algo_spec_flash;
extern struct algo_t algo_spec_eeprom;
extern struct algo_t algo_svec_app_flash;
extern struct algo_t algo_svec_boot_flash;
extern struct algo_t algo_svec_fpga;
extern struct algo_t algo_sis83k_flash;
extern struct algo_t algo_ctra_flash;
extern struct algo_t algo_svec_app_direct;
extern struct algo_t algo_afcz_flash;
extern struct algo_t algo_generic_vme;
extern struct algo_t algo_wr2rf_flash;
extern struct algo_t algo_zynqus_flash;
extern struct algo_t algo_wren_vme_flash;
extern struct algo_t algo_mena25;
extern struct algo_t algo_eda_02175;

void udelay(uint32_t microseconds);

struct spi_flash_chip_t *spi_flash_detect(struct spi_bus_t *bus,
                                          struct spi_flash_chip_t **candidates,
                                          unsigned nbr_candidates);

uint32_t spi_flash_read_id(struct spi_bus_t *bus);
void spi_flash_wait_completion(struct spi_flash_chip_t *chip);
void spi_flash_erase_sector(struct spi_flash_chip_t *chip, uint32_t addr);
void spi_flash_erase_bulk(struct spi_flash_chip_t *chip);
void spi_flash_write_enable(struct spi_flash_chip_t *chip);
void spi_flash_program_page(struct spi_flash_chip_t *chip, uint32_t addr, const uint8_t *data, int size);
int spi_flash_program(struct spi_flash_chip_t *chip, uint32_t addr, const uint8_t *data, int size);
int spi_flash_readback(struct spi_flash_chip_t *chip, uint32_t addr, uint8_t *data, int size);

void flasher_error( const char *fmt, ...);
void flasher_report( const char *fmt, ...);

const char *arg_get_value_str( struct arglist_t *args, const char *opt, const char *default_value );
int arg_is_present( struct arglist_t *args, const char *opt );
struct binary_buffer_t* read_binary_file( const char *filename );
struct binary_buffer_t* read_xilinx_bitfile( const char *filename );
struct binary_buffer_t* read_altera_rpdfile( const char *filename );
struct binary_buffer_t* read_bitfile( const char *filename );
void print_bitfile_info( struct binary_buffer_t *buf );
int write_binary_file(const char *filename, struct binary_buffer_t *buf);

static inline uint8_t reverse_bits8(uint8_t x)
{
	x = ((x >> 1) & 0x55) | ((x & 0x55) << 1);
	x = ((x >> 2) & 0x33) | ((x & 0x33) << 2);
	x = ((x >> 4) & 0x0f) | ((x & 0x0f) << 4);

	return x;
}

#endif
