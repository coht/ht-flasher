
/*
 * Copyright (C) 2013-2018 CERN (www.cern.ch)
 * Author: Tomasz Włostowski <tomasz.wlostowski@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/errno.h>

#include <ual.h>
#include <unistd.h>

#include "ht_flasher.h"

/* Registers for GN4124 access */
enum {
	/* page 106 */
	GNPPCI_MSI_CONTROL	= 0x48,		/* actually, 3 smaller regs */
	GNPPCI_MSI_ADDRESS_LOW	= 0x4c,
	GNPPCI_MSI_ADDRESS_HIGH	= 0x50,
	GNPPCI_MSI_DATA		= 0x54,

	GNPCI_SYS_CFG_SYSTEM	= 0x800,

	/* page 130 ff */
	GNINT_CTRL		= 0x810,
	GNINT_STAT		= 0x814,
	GNINT_CFG_0		= 0x820,
	GNINT_CFG_1		= 0x824,
	GNINT_CFG_2		= 0x828,
	GNINT_CFG_3		= 0x82c,
	GNINT_CFG_4		= 0x830,
	GNINT_CFG_5		= 0x834,
	GNINT_CFG_6		= 0x838,
	GNINT_CFG_7		= 0x83c,
#define GNINT_CFG(x) (GNINT_CFG_0 + 4 * (x))

	/* page 146 ff */
	GNGPIO_BASE = 0xA00,
	GNGPIO_BYPASS_MODE	= GNGPIO_BASE,
	GNGPIO_DIRECTION_MODE	= GNGPIO_BASE + 0x04, /* 0 == output */
	GNGPIO_OUTPUT_ENABLE	= GNGPIO_BASE + 0x08,
	GNGPIO_OUTPUT_VALUE	= GNGPIO_BASE + 0x0C,
	GNGPIO_INPUT_VALUE	= GNGPIO_BASE + 0x10,
	GNGPIO_INT_MASK		= GNGPIO_BASE + 0x14, /* 1 == disabled */
	GNGPIO_INT_MASK_CLR	= GNGPIO_BASE + 0x18, /* irq enable */
	GNGPIO_INT_MASK_SET	= GNGPIO_BASE + 0x1C, /* irq disable */
	GNGPIO_INT_STATUS	= GNGPIO_BASE + 0x20,
	GNGPIO_INT_TYPE		= GNGPIO_BASE + 0x24, /* 1 == level */
	GNGPIO_INT_VALUE	= GNGPIO_BASE + 0x28, /* 1 == high/rise */
	GNGPIO_INT_ON_ANY	= GNGPIO_BASE + 0x2C, /* both edges */

	/* page 158 ff */
	FCL_BASE		= 0xB00,
	FCL_CTRL		= FCL_BASE,
	FCL_STATUS		= FCL_BASE + 0x04,
	FCL_IODATA_IN		= FCL_BASE + 0x08,
	FCL_IODATA_OUT		= FCL_BASE + 0x0C,
	FCL_EN			= FCL_BASE + 0x10,
	FCL_TIMER_0		= FCL_BASE + 0x14,
	FCL_TIMER_1		= FCL_BASE + 0x18,
	FCL_CLK_DIV		= FCL_BASE + 0x1C,
	FCL_IRQ			= FCL_BASE + 0x20,
	FCL_TIMER_CTRL		= FCL_BASE + 0x24,
	FCL_IM			= FCL_BASE + 0x28,
	FCL_TIMER2_0		= FCL_BASE + 0x2C,
	FCL_TIMER2_1		= FCL_BASE + 0x30,
	FCL_DBG_STS		= FCL_BASE + 0x34,

	FCL_FIFO		= 0xE00,

	PCI_SYS_CFG_SYSTEM	= 0x800
};

#define GPIO_DIRECTION_MODE 0xA04
#define GPIO_OUTPUT_ENABLE 0xA08
#define GPIO_OUTPUT_VALUE 0xA0C
#define GPIO_INPUT_VALUE 0xA10


#define SPRI_CLKOUT 0
#define SPRI_DATAOUT 1
#define SPRI_CONFIG 2
#define SPRI_DONE 3
#define SPRI_XI_SWAP 4
#define SPRI_STATUS 5

#define GPIO_SPRI_DIN 13
#define GPIO_FLASH_CS 12

#define GPIO_BOOTSEL0 15
#define GPIO_BOOTSEL1 14

#define SPI_DELAY 5

#define GENNUM_FLASH 1
#define GENNUM_FPGA  2
#define FPGA_FLASH   3


static int do_reboot = 0;
static int do_write = 0;
static const char *golden_path = "spec-golden.bin";

struct spec_spi_priv_t {
        struct ual_bar_tkn* bar4;
};

static uint32_t gennum_readl(struct spi_bus_t *bus, uint32_t addr)
{
        struct ual_bar_tkn* bar = ((struct spec_spi_priv_t*)bus->priv)->bar4;
	uint32_t d = ual_readl( bar, addr );
	//printf("readl 0x%x %x\n", addr, d );
        return d;
}

static void gennum_writel(struct spi_bus_t *bus, uint32_t data, uint32_t addr)
{
        struct ual_bar_tkn* bar = ((struct spec_spi_priv_t*)bus->priv)->bar4;
        ual_writel( bar, addr, data );
}

static void gpio_set1(struct spi_bus_t *bus, uint32_t addr, uint8_t bit)
{
	uint32_t reg;

	reg = gennum_readl(bus, addr);
	reg |= (1 << bit);
	gennum_writel(bus, reg, addr);
}

static void gpio_set0(struct spi_bus_t *bus,uint32_t addr, uint8_t bit)
{
	uint32_t reg;

	reg = gennum_readl(bus, addr);
	reg &= ~(1 << bit);
	gennum_writel(bus, reg, addr);
}

static uint8_t gpio_get(struct spi_bus_t *bus, uint32_t addr, uint8_t bit)
{
	return (gennum_readl(bus, addr) & (1 << bit)) ? 1 : 0;
}

static void gpio_init(struct spi_bus_t *bus)
{
	gennum_writel(bus, 0x00000004, FCL_IODATA_OUT);// FCL outputs to 0 except for PROG_B (1)
	gennum_writel(bus, 0x0000001f, FCL_EN);// FCL output enable
	gennum_writel(bus, 0x00000001, FCL_CTRL);// FCL mode

	gennum_writel(bus, 0x00002000, GPIO_DIRECTION_MODE); // GPIO direction (1=input)
	gennum_writel(bus, 0x0000D000, GPIO_OUTPUT_ENABLE); // GPIO output enable
	gennum_writel(bus, 0x00000000, GPIO_OUTPUT_VALUE); // GPIO output to 0

	gpio_set1(bus, GPIO_OUTPUT_VALUE, GPIO_FLASH_CS);

	//printf("FCL_EN %x\n", gennum_readl( bus, FCL_EN ));
}

static void spec_gpio_bootselect(struct spi_bus_t *bus, uint8_t select)
{
	switch(select){

	case GENNUM_FLASH:
		gpio_set0(bus, GPIO_OUTPUT_VALUE, GPIO_BOOTSEL0);
		gpio_set1(bus, GPIO_OUTPUT_VALUE, GPIO_BOOTSEL1);
		break;

	case GENNUM_FPGA:
		gpio_set1(bus, GPIO_OUTPUT_VALUE, GPIO_BOOTSEL0);
		gpio_set0(bus, GPIO_OUTPUT_VALUE, GPIO_BOOTSEL1);
		break;

	case FPGA_FLASH:
		gennum_writel(bus, 0x00000000, FCL_EN);// FCL output all disabled
		gpio_set1(bus, GPIO_OUTPUT_VALUE, GPIO_BOOTSEL0);
//		gpio_set1(GPIO_OUTPUT_VALUE, GPIO_BOOTSEL1);
		break;

	default:
		break;
	}
}

static uint8_t spec_spi_read8(struct spi_bus_t *bus)
{
	uint8_t rx=0;
	int i;

	gpio_set0(bus, FCL_IODATA_OUT, SPRI_CLKOUT);
	for(i = 0; i < 8;i++){
		udelay(SPI_DELAY);

		rx <<= 1;
		if (gpio_get(bus, GPIO_INPUT_VALUE, GPIO_SPRI_DIN))
			rx |= 1;

		udelay(SPI_DELAY);
		gpio_set1(bus, FCL_IODATA_OUT, SPRI_CLKOUT);
		udelay(SPI_DELAY);
		gpio_set0(bus, FCL_IODATA_OUT, SPRI_CLKOUT);
	}
	udelay(SPI_DELAY);
	return rx;
}

static void spec_spi_write8(struct spi_bus_t *bus, uint8_t tx)
{
	int i;

	//printf("write8 %02x\n", tx);

	gpio_set0(bus, FCL_IODATA_OUT, SPRI_CLKOUT);
	for (i = 0; i < 8;i++) {

		if (tx & 0x80)
			gpio_set1(bus, FCL_IODATA_OUT, SPRI_DATAOUT);
		else
			gpio_set0(bus, FCL_IODATA_OUT, SPRI_DATAOUT);

		tx<<=1;

		udelay(SPI_DELAY);
		gpio_set1(bus, FCL_IODATA_OUT, SPRI_CLKOUT);
		udelay(SPI_DELAY);
		gpio_set0(bus, FCL_IODATA_OUT, SPRI_CLKOUT);
	}
	udelay(SPI_DELAY);
}

static void spec_spi_set_cs( struct spi_bus_t* bus, int value )
{
        if (value)
                gpio_set0(bus, GPIO_OUTPUT_VALUE, GPIO_FLASH_CS);
        else
                gpio_set1(bus, GPIO_OUTPUT_VALUE, GPIO_FLASH_CS);
}



static int spec_spi_init( struct spi_bus_t *bus, struct arglist_t* args )
{
        const char *device = arg_get_value_str( args, "d", NULL );
        const char *golden = arg_get_value_str( args, "g", NULL );

        if (golden)
                golden_path = golden;

        if (!device) {
                flasher_error( "Expected PCI bus/device ID.\n" );
                return -1;
        }

        struct ual_desc_pci desc;

	int b, d;
	int i = sscanf(device, "%x:%x", &b, &d);
	if (i == 2) {
		b &= 0xFF;
		d &= 0xFF;
		desc.devid = (b << 16) | (d << 8);
	}
	else
	{
		flasher_error("Bus/device ID must be in bus:dev format.\n");
		return -1;
	}

        desc.data_width = 4;
	desc.bar = 4; /**< PCI Base Address Register to access */
	desc.size = 0x1000; /**< number of bytes to maps (PAGE_SIZE aligned) */
	desc.offset = 0;
        desc.flags = 0;


        struct ual_bar_tkn *ual = ual_open( UAL_BUS_PCI,  &desc);

        if (!ual)
        {
                flasher_error("Can't initialize UAL: %s\n", ual_strerror(errno));
                return -1;
        }

        struct spec_spi_priv_t *priv = malloc(sizeof(struct spec_spi_priv_t));
        priv->bar4 = ual;
        bus->priv = priv;

        return 0;
}

static int spec_spi_release( struct spi_bus_t *bus )
{
        struct spec_spi_priv_t *priv = bus->priv;

        ual_close( priv->bar4 );
        free(priv);

        return 0;
}

static void spec_spi_udelay( struct spi_bus_t *bus, int us )
{
        udelay(us);
}

static struct spi_bus_t spi_bus_spec_flash =
{
        spec_spi_init,
        spec_spi_release,
        spec_spi_set_cs,
        spec_spi_read8,
        spec_spi_write8,
        spec_spi_udelay
};



static char *filename;
static struct spi_bus_t* const spec_spi = &spi_bus_spec_flash;

static int spec_flash_init(struct algo_t *self, struct arglist_t *args )
{
	do_write = arg_is_present(args, "w");
	do_reboot = arg_is_present(args, "f");

	if (do_write)
	{
		filename = (char *)arg_get_value_str(args, "w", NULL);
		if( !filename )
		{
			flasher_error("Filename expected.\n");
			return -1;
		}
	}

	if( spec_spi->init( spec_spi, args ) < 0)
		return -1;

	return 0;
}

static void spec_flash_help( struct algo_t *self)
{
	flasher_report( "Programs SPEC main flash with a binary FPGA bitstream.\n");
	flasher_report( "\nUsage: ht-flasher -d bus:dev [-w filename.bit/bin] [-g golden.bin] [-f]\n");
	flasher_report( "    -d bus:dev   - the PCI bus/device of the SPEC card to program (hex form, e.g. 0d:00)\n");
	flasher_report( "    -w file.bin  - programs the file contents into the flash\n");
        flasher_report( "    -g golden.bin  - golden image to use for programming\n");
	flasher_report( "    -f           - forces reconfiguration of the FPGA from flash\n");
}

static uint32_t unaligned_bitswap_le32(const uint32_t *ptr32)
{
	static uint32_t tmp32;
	static uint8_t *tmp8 = (uint8_t *) &tmp32;
	static uint8_t *ptr8;

	ptr8 = (uint8_t *) ptr32;

	*(tmp8 + 0) = reverse_bits8(*(ptr8 + 0));
	*(tmp8 + 1) = reverse_bits8(*(ptr8 + 1));
	*(tmp8 + 2) = reverse_bits8(*(ptr8 + 2));
	*(tmp8 + 3) = reverse_bits8(*(ptr8 + 3));

	return tmp32;
}

static int loader_low_level(struct spi_bus_t *bus, const void *data, int size8)
{
	int size32 = (size8 + 3) >> 2;
	const uint32_t *data32 = data;
	int ctrl = 0, i, done = 0, wrote = 0;

	/* configure Gennum GPIO to select GN4124->FPGA configuration mode */
	gpio_set0( bus, GPIO_DIRECTION_MODE, GPIO_BOOTSEL0);
	gpio_set0( bus, GPIO_DIRECTION_MODE, GPIO_BOOTSEL1);
	gpio_set1( bus, GNGPIO_OUTPUT_ENABLE, GPIO_BOOTSEL0);
	gpio_set1( bus, GNGPIO_OUTPUT_ENABLE, GPIO_BOOTSEL1);
	gpio_set1( bus, GPIO_OUTPUT_VALUE, GPIO_BOOTSEL0);
	gpio_set0( bus, GPIO_OUTPUT_VALUE, GPIO_BOOTSEL1);


	gennum_writel(bus, 0x00, FCL_CLK_DIV);
	gennum_writel(bus, 0x40, FCL_CTRL); /* Reset */
	i = gennum_readl(bus, FCL_CTRL);
	if (i != 0x40) {
		flasher_error( "%s: %i: error\n", __func__, __LINE__);
		return -EIO;
	}
	gennum_writel(bus, 0x00, FCL_CTRL);

	gennum_writel(bus, 0x00, FCL_IRQ); /* clear pending irq */

	switch(size8 & 3) {
	case 3: ctrl = 0x116; break;
	case 2: ctrl = 0x126; break;
	case 1: ctrl = 0x136; break;
	case 0: ctrl = 0x106; break;
	}
	gennum_writel(bus, ctrl, FCL_CTRL);

	gennum_writel(bus, 0x00, FCL_CLK_DIV); /* again? maybe 1 or 2? */

	gennum_writel(bus, 0x00, FCL_TIMER_CTRL); /* "disable FCL timr fun" */

	gennum_writel(bus, 0x10, FCL_TIMER_0); /* "pulse width" */
	gennum_writel(bus, 0x00, FCL_TIMER_1);

	/*
	 * Set delay before data and clock is applied by FCL
	 * after SPRI_STATUS is	detected being assert.
	 */
	gennum_writel(bus, 0x08, FCL_TIMER2_0); /* "delay before data/clk" */
	gennum_writel(bus, 0x00, FCL_TIMER2_1);
	gennum_writel(bus, 0x17, FCL_EN); /* "output enable" */

	ctrl |= 0x01; /* "start FSM configuration" */
	gennum_writel(bus, ctrl, FCL_CTRL);

	while(size32 > 0)
	{
		/* Check to see if FPGA configuation has error */
		i = gennum_readl(bus, FCL_IRQ);
		if ( (i & 8) && wrote) {
			done = 1;
		} else if ( (i & 0x4) && !done) {
			return -EIO;
		}

		/* Wait until at least 1/2 of the fifo is empty */
		while (gennum_readl(bus, FCL_IRQ)  & (1<<5))
			;

		/* Write a few dwords into FIFO at a time. */
		for (i = 0; size32 && i < 32; i++) {
			gennum_writel(bus, unaligned_bitswap_le32(data32),
				  FCL_FIFO);
			data32++; size32--; wrote++;
		}
	}

	gennum_writel(bus, 0x186, FCL_CTRL); /* "last data written" */

	/* Checking for the "interrupt" condition is left to the caller */
	return wrote;
}

static void waitdone_low_level(struct spi_bus_t *bus)
{
	while ( (gennum_readl(bus, FCL_IRQ) & 0x8) == 0 )
		usleep(100);
}

/* After programming, we fix gpio lines so pci can access the flash */
static void gpiofix_low_level(struct spi_bus_t *bus)
{
	gpio_set0(bus, GNGPIO_OUTPUT_VALUE, GPIO_BOOTSEL0);
	gpio_set0(bus, GNGPIO_OUTPUT_VALUE, GPIO_BOOTSEL1);
	gpio_set0(bus, GNGPIO_OUTPUT_ENABLE, GPIO_BOOTSEL0);
	gpio_set0(bus, GNGPIO_OUTPUT_ENABLE, GPIO_BOOTSEL1);
}

static void loader_reset_fpga(struct spi_bus_t *bus)
{
	uint32_t reg;

	/* After reprogramming, reset the FPGA using the gennum register */
	reg = gennum_readl(bus, GNPCI_SYS_CFG_SYSTEM);
	/*
	 * This _fucking_ register must be written with extreme care,
	 * becase some fields are "protected" and some are not. *hate*
	 */
	gennum_writel(bus, (reg & ~0xffff) | 0x3fff, GNPCI_SYS_CFG_SYSTEM);
	gennum_writel(bus, (reg & ~0xffff) | 0x7fff, GNPCI_SYS_CFG_SYSTEM);
}

static int spec_flash_run(struct algo_t *self)
{
        static struct spi_flash_chip_t *candidates[] = {
                &spi_flash_chip_m25p32,  // spec45t
                &spi_flash_chip_w25q64,  // spec150
        };
	struct spi_flash_chip_t *flash = NULL;

        flash = spi_flash_detect(spec_spi, candidates,
                                 sizeof(candidates) / sizeof(candidates[0]));
	if (!flash)
		return -1;

	if (do_write)
	{
                struct binary_buffer_t *golden = read_binary_file(golden_path);
		struct binary_buffer_t *data = read_bitfile(filename);

		if(!data )
		{
			flasher_error("Can't open firmware file '%s'\n", filename);
			return -1;
		}

                if (!golden)
                {
			flasher_error("Can't open golden file '%s'\n", golden_path);
			return -1;
		}

		print_bitfile_info(data);
		flasher_report ( "Loading SPEC Golden Bitstream to the FPGA...\n");

		int rv = loader_low_level(spec_spi, golden->data, golden->size);
		waitdone_low_level(spec_spi);
		gpiofix_low_level(spec_spi);
		loader_reset_fpga(spec_spi);

                if (rv < 0)
                        return -1;
		gpio_init(spec_spi );

		gpio_set0( spec_spi, GPIO_OUTPUT_VALUE, GPIO_BOOTSEL0 );
		gpio_set0( spec_spi, GPIO_OUTPUT_VALUE, GPIO_BOOTSEL1 );

		spec_gpio_bootselect( spec_spi, GENNUM_FLASH );

		return spi_flash_program( flash, 0, data->data, data->size );
	} else if (do_reboot) {
		flasher_report( "Rebooting the SPEC FPGA...");

		spec_gpio_bootselect( spec_spi, FPGA_FLASH );

		gennum_writel(spec_spi, 0, FCL_CTRL);
		gennum_writel(spec_spi, 0x7, FCL_EN); // enable PROGRAM_B as GPIO
		gennum_writel(spec_spi, 0x0, FCL_IODATA_OUT); // PROGRAM_B = 0
		usleep(10);
		gennum_writel(spec_spi, 0x4, FCL_IODATA_OUT); // PROGRAM_B = 1
		int timeout = 15;
		while(timeout--)
		{
		    sleep(1);
		    flasher_report(".");
		    if( gennum_readl(spec_spi, FCL_STATUS) & (1<<3) )
		    {
			flasher_report(" OK\n");
			return 0;
		    }
		}

		flasher_error("\nTime out waiting for DONE signal!\n");

		return -1;
	} else {
		flasher_error( "No operation specified. Exiting.\n");
		return 0;
	}
}

static int spec_flash_release(struct algo_t *self)
{
        return 0;
}

struct algo_t algo_spec_flash = {
	"spec",
	"SPI Flash for bitstream storage on the SPEC carrier",

	spec_flash_init,
	spec_flash_help,
	spec_flash_run,
	spec_flash_release
};


static int spec_fpga_init(struct algo_t *self, struct arglist_t *args)
{
	filename = (char *) arg_get_value_str( args, "w", NULL );

	if( !filename )
	{
		flasher_error("Filename expected.\n");
		return -1;
	}

	if( spec_spi->init( spec_spi, args ) < 0)
		return -1;

	return 0;
}

static void spec_fpga_help(struct algo_t *self)
{
	flasher_report( "Programs SPEC FPGA with a binary FPGA bitstream.\n");
	flasher_report( "\nUsage: ht-flasher -d bus:dev -w <filename.bin>\n");
}


static int spec_fpga_run(struct algo_t *self)
{
	struct binary_buffer_t *data = read_binary_file( filename );

        if (data == NULL) {
                flasher_error("cannot read %s: %m\n", filename);
                return -1;
        }
	int rv = loader_low_level(spec_spi, data->data, data->size);
	waitdone_low_level(spec_spi);
	gpiofix_low_level(spec_spi);
	loader_reset_fpga(spec_spi);

        if (rv < 0)
                return -1;
	return 0;
}

static int spec_fpga_release(struct algo_t *self)
{
        return 0;
}

struct algo_t algo_spec_fpga = {
	"spec-fpga",
	"Direct FPGA upload on the SPEC",

	spec_fpga_init,
	spec_fpga_help,
	spec_fpga_run,
	spec_fpga_release
};
