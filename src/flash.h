/*
 * Copyright (C) 2013 CERN (www.cern.ch)
 * Author: Tomasz Włostowski <tomasz.wlostowski@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 *
 * flash id and commands
 */

#define ID_S25FL128     0x012018
#define ID_S25FL128L    0x016018
#define ID_N25Q128      0x20ba18
#define ID_N25Q256      0x20ba19
#define ID_M25QU512     0x20bb20
#define ID_M25P128      0x202018
#define ID_M25P32       0x202016
#define ID_MX25L25635F  0xC22019
#define ID_W25Q64       0xEF4017
#define ID_EPCS64       0x202017

/* M25Pxxx SPI flash commands */
#define FLASH_WREN 0x06
#define FLASH_WRDI 0x04
#define FLASH_RDID 0x9F
#define FLASH_RDSR 0x05
#define FLASH_WRSR 0x01
#define FLASH_READ 0x03
#define FLASH_FAST_READ 0x0B
#define FLASH_PP 0x02
#define FLASH_SE 0xD8
#define FLASH_BE 0xC7

#define FLASH_RESET_ENA 0x66
#define FLASH_RESET_MEM 0x99
#define FLASH_PRG_QUAD 0x32  /* 1-1-4 */
#define FLASH_OFAST_READ 0x6b /* 1-1-4 */
#define FLASH_IOFAST_READ 0xeb /* 1-4-4 */

/* Large (32-bit address) SPI flash commands */
#define FLASH_EN4B 0xB7 /* Enable */
#define FLASH_EX4B 0xE9 /* Exit */

