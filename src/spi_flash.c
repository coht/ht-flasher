
/*
 * Copyright (C) 2013 CERN (www.cern.ch)
 * Author: Tomasz Włostowski <tomasz.wlostowski@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 *
 * svec-flasher: a trivial VME-SPI flasher application.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "ht_flasher.h"
#include "flash.h"

static void spi_flash_sel_addr_mode(struct spi_flash_chip_t *chip)
{
	struct spi_bus_t *bus = chip->bus;

	/* enable 32-bit addressing mode for 256 Mbit chips */
	if (chip->size >= 0x2000000)
	{
		bus->set_cs(bus, 1);
		bus->write8(bus, FLASH_EN4B);
		bus->set_cs(bus, 0);
	}
}

static void spi_flash_addr(struct spi_flash_chip_t* chip, uint32_t addr)
{
	struct spi_bus_t *bus = chip->bus;

	/* 256 Mbit chips use 4-bit address */
	if (chip->size >= 0x2000000)
	{
		bus->write8(bus, (addr >> 24) & 0xff);
	}

	bus->write8(bus, (addr >> 16) & 0xff);
	bus->write8(bus, (addr >> 8) & 0xff);
	bus->write8(bus, addr & 0xff);
}

uint32_t spi_flash_read_id(struct spi_bus_t *bus)
{
	uint32_t val = 0;

	/* make sure the flash is in known state (idle) */
	bus->set_cs(bus, 0);
	bus->delay_us(bus, 10);
	bus->set_cs(bus, 1);
	bus->delay_us(bus, 10);

	bus->set_cs(bus, 1);
	bus->write8(bus, FLASH_RDID);
	val = (bus->read8(bus) << 16);
	val += (bus->read8(bus) << 8);
	val += bus->read8(bus);
	bus->set_cs(bus, 0);

	return val;
}

struct spi_flash_chip_t *
spi_flash_detect(struct spi_bus_t *bus,
                 struct spi_flash_chip_t **candidates,
                 unsigned nbr_candidates)
{
	struct spi_flash_chip_t *flash = NULL;
	int i;
	uint32_t id = spi_flash_read_id(bus);

	for (i = 0; i < nbr_candidates; i++) {
                if (id == candidates[i]->id) {
			flash = candidates[i];
                        flash->bus = bus;
                        return flash;
		}
	}

        flasher_error( "Unknown flash type: %x\n", id );
        return NULL;
}

void spi_flash_wait_completion(struct spi_flash_chip_t *chip)
{
	struct spi_bus_t *bus = chip->bus;

	int not_done = 1;

	while (not_done)
	{
		bus->set_cs(bus, 1);
		bus->write8(bus, FLASH_RDSR); /* Read Status register */
		uint8_t stat = bus->read8(bus);
		not_done = (stat & 0x01);
		bus->set_cs(bus, 0);
	}
}

void spi_flash_erase_sector(struct spi_flash_chip_t *chip, uint32_t addr)
{
	struct spi_bus_t *bus = chip->bus;

	spi_flash_sel_addr_mode(chip);

	spi_flash_write_enable(chip);

	bus->set_cs(bus, 1);
	bus->write8(bus, FLASH_SE);
	spi_flash_addr(chip, addr);
	bus->set_cs(bus, 0);
	spi_flash_wait_completion(chip);
}

void spi_flash_erase_bulk(struct spi_flash_chip_t *chip)
{
	int n = 0;
	int p = 0;
	int data = 0;
	uint64_t size = chip->size;
	uint8_t readed_data = 0xff;

	struct spi_bus_t *bus = (struct spi_bus_t *) chip->bus;

	spi_flash_write_enable(chip);
	bus->set_cs(bus, 1);
	bus->write8(bus, FLASH_BE);
	bus->set_cs(bus, 0);
	spi_flash_wait_completion(chip);

	flasher_report("Done! Flash memory erased.\n");

	//verification
	bus->set_cs(bus, 1);
	bus->write8(bus, FLASH_READ);
	spi_flash_addr(chip, 0);

	for (n = 0, p = data; n < size; p++, n++)
	{
		uint8_t d = bus->read8(bus);
		//flasher_report("\rReaded: 0x%02x  \n", d);

		if( (n % 10000) == 0 )
		{
			flasher_report("\rVerification: %.0f%%                      ", (double)n / (double) size * 100.0);
		}

		if (d != readed_data)
		{
			flasher_report("\rReaded: %f  \n", d);
			flasher_report(
					"Verification failed at offset 0x%06x (is: 0x%02x, should be: 0x%02x)\n.",
					0 + n, d, readed_data);
			bus->set_cs(bus, 0);
			return -1;
		}
	}
	flasher_report("\rVerification: %.0f%%          \n", 100.0);
	bus->set_cs(bus, 0);




}

void spi_flash_write_enable(struct spi_flash_chip_t *chip)
{
	struct spi_bus_t *bus = chip->bus;

	bus->set_cs(bus, 1);
	bus->write8(bus, FLASH_WREN);
	bus->set_cs(bus, 0);
}

void spi_flash_program_page(struct spi_flash_chip_t *chip, uint32_t addr, const uint8_t *data, int size)
{
	struct spi_bus_t *bus = chip->bus;
	int i;

	spi_flash_sel_addr_mode(chip);

	spi_flash_write_enable(chip);

	bus->set_cs(bus, 1);
	bus->write8(bus, FLASH_PP);				 /* Page Program */
	spi_flash_addr(chip, addr);

	for (i = 0; i < size; i++)
		bus->write8(bus, data[i]);
	for (; i < chip->page_size; i++)
		bus->write8(bus, 0xff);
	bus->set_cs(bus, 0);
	spi_flash_wait_completion(chip);
}

int spi_flash_program(struct spi_flash_chip_t *chip, uint32_t addr, const uint8_t *data, int size)
{
	struct spi_bus_t *bus = chip->bus;

	int n = 0;
	int sector_map[chip->size / chip->sector_size];
	memset(sector_map, 0, sizeof(sector_map));
	const uint8_t *p = data;

	uint32_t id = spi_flash_read_id(bus);


	flasher_report( "Check flash ID : 0x%x (expecting 0x%x = %s).\n", id, chip->id, chip->name );

	if( id != chip->id )
	{
		flasher_error("No matching flash memory found.\n");
		return -1;
	}

	uint32_t remaining = size;

	while (n < size)
	{
		int plen = (remaining > chip->page_size ? chip->page_size : remaining);
		int sector = ((addr + n) / chip->sector_size);

		if (!sector_map[sector])
		{
			flasher_report( "Erasing sector 0x%x                \r",
					addr + n);
			spi_flash_erase_sector(chip, addr + n);
			sector_map[sector] = 1;
		}

		spi_flash_program_page(chip, addr + n, data + n, plen);

		if( (n % 20) == 0 )
		flasher_report( "Programming page %d/%d.             \r",
				n / chip->page_size,
				(size + chip->page_size - 1) / chip->page_size - 1);

		n += plen;
		remaining -= plen;
	}

	bus->set_cs(bus, 1);
	bus->write8(bus, FLASH_READ);
	spi_flash_addr(chip, addr);

	for (n = 0, p = data; n < size; p++, n++)
	{
		uint8_t d = bus->read8(bus);

		if( (n % 10000) == 0 )
		{
			flasher_report("\rVerification: %.0f%%                      ", (double)n / (double) size * 100.0);
		}

		if (d != *p)
		{
			flasher_report(
					"Verification failed at offset 0x%06x (is: 0x%02x, should be: 0x%02x)\n.",
					addr + n, d, *p);
			bus->set_cs(bus, 0);
			return -1;
		}
	}

	flasher_report("\rVerification: %.0f%%          \n", 100.0);
	bus->set_cs(bus, 0);

	return 0;
}


int spi_flash_readback(struct spi_flash_chip_t *chip, uint32_t addr, uint8_t *data, int size)
{
	struct spi_bus_t *bus = chip->bus;

	int n = 0;

	uint32_t id = spi_flash_read_id(bus);

	flasher_report( "Check flash ID : 0x%x (expecting 0x%x = %s).\n", id, chip->id, chip->name );

	if( id != chip->id )
	{
		flasher_error("No matching flash memory found.\n");
		return -1;
	}

	while (n < size)
	{
		int plen = (size > chip->page_size ? chip->page_size : size);
		int i;

		bus->set_cs(bus, 1);
		bus->write8(bus, FLASH_READ);
		spi_flash_addr(chip, addr + n);


		for (i = 0; i < plen; i++ )
		{
			data[n + i] = bus->read8(bus);
		}

		bus->set_cs(bus, 0);

/*		if( (n % 20) == 0 )
		flasher_report( "Reading page %d/%d.             \r",
				n / chip->page_size,
				(size + chip->page_size - 1) / chip->page_size - 1);*/

		n += plen;
	}

	return 0;
}


struct spi_flash_chip_t spi_flash_chip_m25p32 = {
	"M25P32",
	ID_M25P32,
	0x400000,
	0x10000,
	256,
};

struct spi_flash_chip_t spi_flash_chip_m25p128 = {
	"M25P128",
	ID_M25P128,
	0x1000000,
	0x40000,
	64,
};

struct spi_flash_chip_t spi_flash_chip_s25fl128 = {
	"S25FL128",
	ID_S25FL128,
	0x1000000,
	0x10000,
	256,
};


struct spi_flash_chip_t spi_flash_chip_s25fl128l = {
	"S25FL128L",
	ID_S25FL128L,
	0x1000000,
	0x10000,
	256,
};


struct spi_flash_chip_t spi_flash_chip_mx25l25635f = {
	"MX25L25635F",
	ID_MX25L25635F,
	0x2000000,
	0x10000,
	256,
};


struct spi_flash_chip_t spi_flash_chip_n25q256 = {
	"N25Q256",
	ID_N25Q256,
	0x2000000,
	0x10000,
	256,
};

struct spi_flash_chip_t spi_flash_chip_n25q128 = {
	"N25Q128",
	ID_N25Q128,
	0x1000000,
	0x10000,
	256,
};

struct spi_flash_chip_t spi_flash_chip_m25qu512 = {
	"M25QU512",
	ID_M25QU512,
	0x4000000,
	0x10000,
	256,
};


struct spi_flash_chip_t spi_flash_chip_w25q64 = {
	"W25Q64",
	ID_W25Q64,
	0x800000,
	0x10000,
	256,
};


struct spi_flash_chip_t spi_flash_chip_epcs64 = {
	"EPCS64",
	ID_EPCS64,
	0x800000,
	0x10000,
	256,
};
