/*
 * Copyright (C) 2013-2018 CERN (www.cern.ch)
 *
 * Released according to the GNU GPL, version 2 or any later version.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/errno.h>

#include <ual.h>

#include "ht_flasher.h"
#include "flash.h"

#define	ISR		0x0000000004
#define GQSPI_CFG	0x0000000100
#define GQSPI_ISR	0x0000000104
# define GQSPI_ISR_RX_FIFO_EMPTY (1 << 11)
# define GQSPI_ISR_TX_FIFO_EMPTY (1 << 8)
# define GQSPI_ISR_GEN_FIFO_EMPTY (1 << 7)
# define GQSPI_ISR_RX_FIFO_NOT_EMPTY (1 << 4)
# define GQSPI_ISR_TX_FIFO_FULL (1 << 3)
#define GQSPI_En	0x0000000114
#define	GQSPI_TXD	0x000000011C
#define GQSPI_RXD	0x0000000120
#define GQSPI_GEN_FIFO  0x0000000140
# define GQSPI_GEN_FIFO_TFR (1 << 8)
# define GQSPI_GEN_FIFO_EXP (1 << 9)
# define GQSPI_GEN_FIFO_SPI  (0x1 << 10)
# define GQSPI_GEN_FIFO_DSPI (0x2 << 10)
# define GQSPI_GEN_FIFO_QSPI (0x3 << 10)
# define GQSPI_GEN_FIFO_CS  12
# define GQSPI_GEN_FIFO_CS_L (0x1 << GQSPI_GEN_FIFO_CS)
# define GQSPI_GEN_FIFO_CS_U (0x2 << GQSPI_GEN_FIFO_CS)
# define GQSPI_GEN_FIFO_CS_B (0x3 << GQSPI_GEN_FIFO_CS)
# define GQSPI_GEN_FIFO_BUS 14
# define GQSPI_GEN_FIFO_BUS_L (0x1 << GQSPI_GEN_FIFO_BUS)
# define GQSPI_GEN_FIFO_BUS_U (0x2 << GQSPI_GEN_FIFO_BUS)
# define GQSPI_GEN_FIFO_BUS_B (0x3 << GQSPI_GEN_FIFO_BUS)
# define GQSPI_GEN_FIFO_TX (1 << 16)
# define GQSPI_GEN_FIFO_RX (1 << 17)
# define GQSPI_GEN_FIFO_STRIPE (1 << 18)
# define GQSPI_GEN_FIFO_POLL (1 << 19)
#define GQSPI_SEL	0x0000000144
#define GQSPI_FIFO_CTRL	0x000000014C
# define GQSPI_FIFO_CTRL_RST_GEN_FIFO (1 << 0)
# define GQSPI_FIFO_CTRL_RST_TX_FIFO (1 << 1)
# define GQSPI_FIFO_CTRL_RST_RX_FIFO (1 << 2)

struct zynqus_bus_t {
        struct ual_bar_tkn* bar;
	/* chip select (1, 2 or 3) */
	unsigned cs;
	unsigned base;
};

static uint32_t sys_readl(struct zynqus_bus_t *bus, uint32_t addr)
{
        uint32_t d = ual_readl(bus->bar, bus->base + addr);
        return d;
}

static void sys_writel(struct zynqus_bus_t *bus, uint32_t data, uint32_t addr)
{
        ual_writel(bus->bar, bus->base + addr, data );
}

static void sys_write_cmd(struct zynqus_bus_t *bus, uint32_t data)
{
        //printf("GEN_FIFO<- %08x\n", data);
	sys_writel(bus, data, GQSPI_GEN_FIFO);
}

static uint32_t zynqus_qspi_read_fifo_word(struct zynqus_bus_t *bus)
{
	while (1) {
		unsigned status = sys_readl(bus, GQSPI_ISR);
		//printf("GQSPI ISR=%08x\n", status);
		if (status & GQSPI_ISR_RX_FIFO_NOT_EMPTY) {
			uint32_t d = sys_readl(bus, GQSPI_RXD);
			//printf("GQSPI data: %08x\n", d);
			return d;
		}
	}
}

/* Read LEN bytes from qspi rx fifo */
static int zynqus_qspi_read_fifo(struct zynqus_bus_t *bus,
				 uint8_t *ptr, unsigned len)
{
	while (len > 0) {
		uint32_t d = zynqus_qspi_read_fifo_word(bus);
		for (unsigned i = 4; i > 0 && len > 0; i--, len--) {
			*ptr++ = d & 0xff;
			d >>= 8;
		}
	}

	unsigned status = sys_readl(bus, GQSPI_ISR);
	if ((status & GQSPI_ISR_RX_FIFO_EMPTY)
	    && (status & GQSPI_ISR_TX_FIFO_EMPTY)
	    && (status & GQSPI_ISR_GEN_FIFO_EMPTY))
		return 0;
	printf("zynqus_qspi: fifos not empty\n");
	return -1;
}

static void zynqus_qspi_write_fifo(struct zynqus_bus_t *bus, uint32_t w)
{
	//printf("write_fifo: %08x\n", w);
	while (1) {
		unsigned status = sys_readl(bus, GQSPI_ISR);
		//printf("GQSPI ISR=%08x\n", status);
		if (!(status & GQSPI_ISR_TX_FIFO_FULL)) {
			sys_writel(bus, w, GQSPI_TXD);
			return;
		}
		printf("TX fifo full ISR=%08x\n", status);
		udelay(100000);
	}
}

static void zynqus_qspi_wait(struct zynqus_bus_t *bus)
{
	while (1) {
		unsigned status = sys_readl(bus, GQSPI_ISR);
		if ((status & GQSPI_ISR_RX_FIFO_EMPTY)
		    && (status & GQSPI_ISR_TX_FIFO_EMPTY)
		    && (status & GQSPI_ISR_GEN_FIFO_EMPTY))
			return;
		printf("wait...\n");
		udelay(100000);
	}
}

static void zynqus_qspi_set_cs(struct zynqus_bus_t *bus, int cs)
{
	sys_write_cmd(bus, ((bus->cs << GQSPI_GEN_FIFO_BUS)
			    | (cs ? (bus->cs << GQSPI_GEN_FIFO_CS) : 0)
			    | GQSPI_GEN_FIFO_SPI
			    | 0x04));
}

static void zynqus_qspi_write8(struct zynqus_bus_t *bus, unsigned val)
{
	sys_write_cmd(bus, (GQSPI_GEN_FIFO_TX
			    | (bus->cs << GQSPI_GEN_FIFO_BUS)
			    | (bus->cs << GQSPI_GEN_FIFO_CS)
			    | GQSPI_GEN_FIFO_SPI
			    | val));
}

#if 0
static void zynqus_qspi_write8_x4(struct zynqus_bus_t *bus, unsigned val)
{
	sys_write_cmd(bus, (GQSPI_GEN_FIFO_TX
			    | (bus->cs << GQSPI_GEN_FIFO_BUS)
			    | (bus->cs << GQSPI_GEN_FIFO_CS)
			    | GQSPI_GEN_FIFO_QSPI
			    | val));
}
#endif

/* flags is GQSPI_GEN_FIFO_TX or GQSPI_GEN_FIFO_RX
   ored with GQSPI_GEN_FIFO_SPI or GQSPI_GEN_FIFO_QSPI */
static void zynqus_qspi_set_len(struct zynqus_bus_t *bus,
				unsigned len, uint32_t flags)
{
	if (len >= (1 << 30))
		abort ();
	if (len > 255) {
		for (unsigned i = 30; i >= 8; i--) {
			if (!(len & (1 << i)))
				continue;
			sys_write_cmd(bus, (flags
					    | (bus->cs << GQSPI_GEN_FIFO_BUS)
					    | (bus->cs << GQSPI_GEN_FIFO_CS)
					    | GQSPI_GEN_FIFO_EXP
					    | GQSPI_GEN_FIFO_TFR
					    | i));
		}
	}
	if (len & 0xff)
		sys_write_cmd(bus, (flags
				    | (bus->cs << GQSPI_GEN_FIFO_BUS)
				    | (bus->cs << GQSPI_GEN_FIFO_CS)
				    | GQSPI_GEN_FIFO_TFR
				    | (len & 0xff)));
}

static void zynqus_qspi_set_read_len(struct zynqus_bus_t *bus, unsigned len)
{
	zynqus_qspi_set_len(bus, len, GQSPI_GEN_FIFO_RX | GQSPI_GEN_FIFO_SPI);
}

static void zynqus_qspi_exec(struct zynqus_bus_t *bus)
{
	sys_writel(bus, 0x3000010, GQSPI_CFG);
}

/* Do simple read register or read id command.  Always SPI */
static int zynqus_qspi_read_reg(struct zynqus_bus_t *bus,
				unsigned cmd, unsigned char *dest, unsigned len)
{
	zynqus_qspi_set_cs(bus, 1);
	zynqus_qspi_write8(bus, cmd);
	zynqus_qspi_set_read_len(bus, len);
	zynqus_qspi_set_cs(bus, 0);

	zynqus_qspi_exec(bus);

	return zynqus_qspi_read_fifo(bus, dest, len);
}

static void zynqus_qspi_dummy(struct zynqus_bus_t *bus, unsigned len)
{
	sys_write_cmd(bus, ((bus->cs << GQSPI_GEN_FIFO_BUS)
			    | (bus->cs << GQSPI_GEN_FIFO_CS)
			    | GQSPI_GEN_FIFO_SPI
			    | GQSPI_GEN_FIFO_TFR
			    | len)); /* tx 10 dummy cyc (qspi)*/
}

static void zynqus_qspi_addr_sz(struct zynqus_bus_t *bus,
				uint32_t addr,
				unsigned addr_4b)
{
	if (addr_4b)
		zynqus_qspi_write8(bus, (addr >> 24) & 0xff);

	zynqus_qspi_write8(bus, (addr >> 16) & 0xff);
	zynqus_qspi_write8(bus, (addr >> 8) & 0xff);
	zynqus_qspi_write8(bus, addr & 0xff);
}

static void zynqus_qspi_addr(struct spi_flash_chip_t* chip,
			     struct zynqus_bus_t *bus,
			     uint32_t addr)
{
	/* 256 Mbit chips use 4-bit address */
	zynqus_qspi_addr_sz(bus, addr, chip->size >= 0x2000000);
}

static void zynqus_qspi_address_mode(struct zynqus_bus_t *bus, int cmd)
{
	zynqus_qspi_set_cs(bus, 1);
	zynqus_qspi_write8(bus, cmd);
	zynqus_qspi_set_cs(bus, 0);
	zynqus_qspi_exec(bus);
}

static int zynqus_qspi_read_otp(struct zynqus_bus_t *bus,
				unsigned char *dest, unsigned len)
{
	zynqus_qspi_address_mode(bus, FLASH_EX4B);

	zynqus_qspi_set_cs(bus, 1);
	zynqus_qspi_write8(bus, 0x4b);
	zynqus_qspi_addr_sz(bus, 0, 0);
	zynqus_qspi_dummy(bus, 8);
	zynqus_qspi_set_read_len(bus, len);
	zynqus_qspi_set_cs(bus, 0);

	zynqus_qspi_exec(bus);

	return zynqus_qspi_read_fifo(bus, dest, len);
}

static void zynqus_qspi_dump_data(const char *name,
				  const uint8_t *data, int len)
{
	printf("%s:", name);
	for (int i = 0; i < len; i++)
		printf(" %02x", data[i]);
	printf("\n");
}

static int zynqus_qspi_dump_reg(struct zynqus_bus_t *bus, const char *name,
				unsigned cmd, int len)
{
	uint8_t data[20];

	if (len > sizeof(data))
		return -2;
	if (zynqus_qspi_read_reg(bus, cmd, data, len) < 0)
		return -1;

	zynqus_qspi_dump_data(name, data, len);

	return 0;
}

static int zynqus_qspi_dump_otp(struct zynqus_bus_t *bus, const char *name)
{
	uint8_t data[64];
	unsigned i;

	if (zynqus_qspi_read_otp(bus, data, sizeof(data)) < 0)
		return -1;

	for (i = 0; i < sizeof(data); i += 16) {
		unsigned j;
		if (i % 16 == 0)
			printf("OTP[%02u]:", i);
		for (j = i; j < sizeof(data) && j < i + 16; j++)
			printf (" %02x", data[j]);
		for (; j < i + 16; j++)
			printf ("   ");
		printf ("  ");

		for (j = i; j < sizeof(data) && j < i + 16; j++) {
			unsigned char c = data[j];
			printf ("%c", c >= ' ' && c < 0x7f ? c : '.');
		}
		printf("\n");
	}

	return 0;
}

static int zynqus_qspi_read_data(struct zynqus_bus_t *bus,
				 unsigned addr, unsigned len,
				 unsigned char *dest)
{
	/* Note: parallel flash! */
	addr /= 2;

	zynqus_qspi_set_cs(bus, 1);
	zynqus_qspi_write8(bus, 0xeb); /* tx Quad Fast Read */
	sys_write_cmd(bus, (GQSPI_GEN_FIFO_TX
			    | GQSPI_GEN_FIFO_BUS_B
			    | GQSPI_GEN_FIFO_CS_B
			    | GQSPI_GEN_FIFO_QSPI
			    | ((addr >> 16) & 0xff))); /* tx addr msb (qspi) */
	sys_write_cmd(bus, (GQSPI_GEN_FIFO_TX
			    | GQSPI_GEN_FIFO_BUS_B
			    | GQSPI_GEN_FIFO_CS_B
			    | GQSPI_GEN_FIFO_QSPI
			    | ((addr >> 8) & 0xff))); /* tx addr     (qspi) */
	sys_write_cmd(bus, (GQSPI_GEN_FIFO_TX
			    | GQSPI_GEN_FIFO_BUS_B
			    | GQSPI_GEN_FIFO_CS_B
			    | GQSPI_GEN_FIFO_QSPI
			    | ((addr >> 0) & 0xff))); /* tx addr lsb (qspi) */
	sys_write_cmd(bus, (GQSPI_GEN_FIFO_BUS_B
			    | GQSPI_GEN_FIFO_CS_B
			    | GQSPI_GEN_FIFO_SPI
			    | GQSPI_GEN_FIFO_TFR
			    | 0x0a)); /* tx 10 dummy cyc (qspi)*/
	zynqus_qspi_set_len (bus, len, (GQSPI_GEN_FIFO_RX
					| GQSPI_GEN_FIFO_STRIPE
					| GQSPI_GEN_FIFO_QSPI));
	zynqus_qspi_set_cs(bus, 0);
        zynqus_qspi_exec(bus);

        if (zynqus_qspi_read_fifo(bus, dest, len) < 0)
		return -1;
	return len;
}

static void zynqus_qspi_init(struct zynqus_bus_t *bus)
{
	sys_writel(bus, 1, GQSPI_SEL); /* Select QSPI */

	unsigned cfg = sys_readl(bus, GQSPI_CFG);
	printf("GQSPI CFG: %08x\n", cfg);

	/* IO mode, manual start, rate=/4 */
	sys_writel(bus, 0x20000010, GQSPI_CFG);
	unsigned status = sys_readl(bus, ISR);
	printf("ISR=%08x\n", status);

	while (status & GQSPI_ISR_RX_FIFO_NOT_EMPTY) {
		sys_readl(bus, GQSPI_RXD);
		status = sys_readl(bus, ISR);
	}

	/* Reset FIFOs */
	sys_writel(bus, (GQSPI_FIFO_CTRL_RST_TX_FIFO
			 | GQSPI_FIFO_CTRL_RST_RX_FIFO
			 | GQSPI_FIFO_CTRL_RST_GEN_FIFO), GQSPI_FIFO_CTRL);

	printf("FIFO CTRL: %08x\n", sys_readl(bus, GQSPI_FIFO_CTRL));

	sys_writel(bus, 1, GQSPI_En); /* Enable device */
}

#if 0
static int zynqus_qspi_release(struct zynqus_bus_t *bus )
{
        ual_close(bus->bar);
        free(priv);

        return 0;
}
#endif

static void zynqus_qspi_delay_us(struct zynqus_bus_t *bus, int us)
{
        udelay(us);
}

static void qspi_flash_sel_addr_mode(struct spi_flash_chip_t* chip,
				     struct zynqus_bus_t *bus)
{
	/* enable 32-bit addressing mode for 256 Mbit chips */
	if (chip->size >= 0x2000000)
		zynqus_qspi_address_mode(bus, FLASH_EN4B);
}

#if 0
static void qspi_flash_addr_x4(struct spi_flash_chip_t* chip,
			       struct zynqus_bus_t *bus,
			       uint32_t addr)
{
	/* 256 Mbit chips use 4-bit address */
	if (chip->size >= 0x2000000)
		zynqus_qspi_write8_x4(bus, (addr >> 24) & 0xff);

	zynqus_qspi_write8_x4(bus, (addr >> 16) & 0xff);
	zynqus_qspi_write8_x4(bus, (addr >> 8) & 0xff);
	zynqus_qspi_write8_x4(bus, addr & 0xff);
}
#endif

static uint32_t qspi_flash_read_id(struct spi_flash_chip_t *chip,
				   struct zynqus_bus_t *bus)
{
	uint8_t v[3];

	/* Can only read id from a single flash */
	if (bus->cs == 3)
		return -1;

	/* make sure the flash is in known state (idle) */
	zynqus_qspi_set_cs(bus, 0);
	zynqus_qspi_exec(bus);
	zynqus_qspi_delay_us(bus, 10);
	zynqus_qspi_set_cs(bus, 1);
	zynqus_qspi_exec(bus);
	zynqus_qspi_delay_us(bus, 10);

	zynqus_qspi_write8(bus, FLASH_RDID);
	zynqus_qspi_set_read_len(bus, 3);
	zynqus_qspi_set_cs(bus, 0);

	zynqus_qspi_exec(bus);

	if (zynqus_qspi_read_fifo(bus, v, 3) < 0)
		return -1;

	return (v[0] << 16) | (v[1] << 8) | v[2];
}

static void qspi_flash_wait_completion(struct zynqus_bus_t *bus)
{
	unsigned nflash = bus->cs == 3 ? 2 : 1;
	while (1)
	{
		uint8_t v[2];

		zynqus_qspi_set_cs(bus, 1);
		zynqus_qspi_write8(bus, FLASH_RDSR);

		zynqus_qspi_set_len(bus, nflash, (GQSPI_GEN_FIFO_STRIPE
						  | GQSPI_GEN_FIFO_RX
						  | GQSPI_GEN_FIFO_SPI));
		zynqus_qspi_exec(bus);

		if (zynqus_qspi_read_fifo(bus, v, nflash) < 0) {
			printf("wait for completion error\n");
			break;
		}

		if (v[0] & 0x01)
			continue;
		if (nflash == 2 && (v[1] & 0x01))
			continue;
		break;
	}
	zynqus_qspi_set_cs(bus, 0);
}

static void qspi_flash_write_enable(struct spi_flash_chip_t *chip,
				    struct zynqus_bus_t *bus)
{
	zynqus_qspi_set_cs(bus, 1);
	zynqus_qspi_write8(bus, FLASH_WREN);
	zynqus_qspi_set_cs(bus, 0);
	zynqus_qspi_exec(bus);
}

static void qspi_flash_erase_sector(struct spi_flash_chip_t *chip,
				    struct zynqus_bus_t *bus,
				    uint32_t addr)
{
	qspi_flash_write_enable(chip, bus);

	zynqus_qspi_set_cs(bus, 1);
	zynqus_qspi_write8(bus, FLASH_SE);
	zynqus_qspi_addr(chip, bus, addr);
	zynqus_qspi_set_cs(bus, 0);
	zynqus_qspi_exec(bus);

	qspi_flash_wait_completion(bus);
}

struct word_packer {
	uint32_t d;
	unsigned b;
};

static void word_packer_init(struct word_packer *w)
{
	w->d = 0;
	w->b = 0;
}

/* Write W to the fifo if needed */
static void zynqus_qspi_write_flush(struct zynqus_bus_t *bus,
				    struct word_packer *w)
{
	if (w->b == 0)
		return;
	zynqus_qspi_write_fifo(bus, w->d);
	w->d = 0;
	w->b = 0;
}

static void zynqus_qspi_write_push(struct zynqus_bus_t *bus,
				   struct word_packer *w,
				   unsigned char v)
{
	/* Add a byte */
	if (w->b > 24)
		abort();
	w->d |= v << w->b;
	w->b += 8;

	/* Flush if full */
	if (w->b == 32)
		zynqus_qspi_write_flush(bus, w);
}

static void zynqus_qspi_read_word_packer(struct zynqus_bus_t *bus,
					 struct word_packer *w)
{
	if (w->b != 0)
		return;
	w->d = zynqus_qspi_read_fifo_word(bus);
	w->b = 32;
}

static unsigned char zynqus_qspi_read_fetch(struct zynqus_bus_t *bus,
					    struct word_packer *w)
{
	unsigned char res;

	zynqus_qspi_read_word_packer(bus, w);
	res = w->d & 0xff;
	w->d >>= 8;
	w->b -= 8;

	return res;
}

/* Send data and wait for write completion.
   SIZE bytes from DATA are sent, and then padded to PAGE_SIZE. */
static void zynqus_qspi_program_wait(struct zynqus_bus_t *bus,
				     const uint8_t *data, unsigned size,
				     unsigned page_size)
{
	struct word_packer w;
	unsigned i;

	word_packer_init(&w);
	for (i = 0; i < size; i++)
		zynqus_qspi_write_push(bus, &w, data[i]);
	for (; i < page_size; i++)
		zynqus_qspi_write_push(bus, &w, 0xff);

	zynqus_qspi_write_flush(bus, &w);

	zynqus_qspi_set_cs(bus, 0);
	zynqus_qspi_exec(bus);
	zynqus_qspi_wait(bus);

	qspi_flash_wait_completion(bus);
}

static void qspi_flash_program_page(struct spi_flash_chip_t *chip,
				    struct zynqus_bus_t *bus,
				    uint32_t addr,
				    const uint8_t *data, int size)
{
	unsigned page_size = chip->page_size << 1; /* Parallel */

	qspi_flash_write_enable(chip, bus);

	zynqus_qspi_set_cs(bus, 1);
	zynqus_qspi_write8(bus, FLASH_PRG_QUAD); /* Page Program */
	zynqus_qspi_addr(chip, bus, addr);
	zynqus_qspi_set_len(bus, page_size, (GQSPI_GEN_FIFO_TX
					     | GQSPI_GEN_FIFO_STRIPE
					     | GQSPI_GEN_FIFO_QSPI));

	zynqus_qspi_exec(bus);

	zynqus_qspi_program_wait(bus, data, size, page_size);
}

static int qspi_flash_program(struct spi_flash_chip_t *chip,
			      struct zynqus_bus_t *bus,
			      uint32_t addr, const uint8_t *data, int size)
{
	int n = 0;
	unsigned char sector_map[chip->size / chip->sector_size];
	uint32_t remaining = size;
	unsigned page_size = chip->page_size << 1; /* Parallel */
	struct word_packer w;
	const uint8_t *p;
	unsigned nerr;

	memset(sector_map, 0, sizeof(sector_map));

	bus->cs = 3;

	qspi_flash_sel_addr_mode(chip, bus);

        /* Progam page by page. */
	while (n < size)
	{
		int plen = (remaining > page_size ? page_size : remaining);
		unsigned ad = (addr + n) >> 1;  /* parallel */
		int sector = (ad / chip->sector_size);

		if (!sector_map[sector])
		{
			flasher_report("Erasing sector 0x%x            \r", ad);
			qspi_flash_erase_sector(chip, bus, ad);
			sector_map[sector] = 1;
		}

		if( (n % 20) == 0 )
			flasher_report("Programming page %d/%d.        \r",
				       n / chip->page_size,
				       (size + chip->page_size - 1) / chip->page_size - 1);

		qspi_flash_program_page(chip, bus, ad, data + n, plen);

		remaining -= plen;
		n += plen;
	}

	zynqus_qspi_set_cs(bus, 1);
	zynqus_qspi_write8(bus, FLASH_OFAST_READ);
	zynqus_qspi_addr(chip, bus, addr >> 1); /* parallel */
	zynqus_qspi_set_len (bus, 8, GQSPI_GEN_FIFO_QSPI); /* dummy */
	zynqus_qspi_set_len (bus, size, (GQSPI_GEN_FIFO_RX
					 | GQSPI_GEN_FIFO_STRIPE
					 | GQSPI_GEN_FIFO_QSPI));

	word_packer_init(&w);
	nerr = 0;

	for (n = 0, p = data; n < size; p++, n++)
	{
		uint8_t d = zynqus_qspi_read_fetch(bus, &w);

		if( (n % 10000) == 0 )
		{
			flasher_report("Verification: %.0f%%        \r",
				       (double)n / (double) size * 100.0);
		}

		if (d != *p && nerr < 10)
		{
			flasher_report(
					"Verification failed at offset 0x%06x (is: 0x%02x, should be: 0x%02x)\n.",
					addr + n, d, *p);
			nerr++;
		}
	}

	flasher_report("Verification: %.0f%%          \n", 100.0);
	zynqus_qspi_set_cs(bus, 0);

	return nerr ? -1 : 0;
}


static const char *filename;
static int do_read, do_write, do_info, do_reset, do_wotp;
static unsigned address, length;
static struct zynqus_bus_t *bus;

static int zynqus_flash_init_common(struct arglist_t *args )
{
	do_read = arg_is_present(args, "r");
	do_write = arg_is_present(args, "w");
	do_info = arg_is_present(args, "i");
	do_wotp = arg_is_present(args, "wotp1");
	do_reset = arg_is_present(args, "reset");

	if (do_read + do_write + do_info + do_wotp > 1) {
		flasher_error("need one option between -r, -w, -i, -wotp\n");
		return -1;
	}

	if (do_read)
		filename = (char *)arg_get_value_str(args, "r", NULL);

	if (do_write)
		filename = (char *)arg_get_value_str(args, "w", NULL);

	if (do_wotp == 1)
		filename = (char *)arg_get_value_str(args, "wotp1", NULL);

	if (arg_is_present(args, "o")) {
		char *arg = (char *)arg_get_value_str(args, "o", NULL);
		char *e;
		address = strtoul(arg, &e, 0);
		if (*e) {
			flasher_error("incorrect option -o\n");
			return -1;
		}
	}

	if (arg_is_present(args, "S")) {
		char *arg = (char *)arg_get_value_str(args, "S", NULL);
		char *e;
		length = strtoul(arg, &e, 0);
		if (*e) {
			flasher_error("incorrect option -s\n");
			return -1;
		}
	}

	return 0;
}

static int zynqus_flash_init_pcie( struct algo_t *self, struct arglist_t *args )
{
        const char *device = arg_get_value_str(args, "d", NULL);
        struct ual_desc_pci desc;
	struct ual_bar_tkn *ual;
	int b, d;
	int i;

	if (zynqus_flash_init_common(args) < 0)
		return -1;

        if (!device) {
                flasher_error( "Expected PCI bus/device ID.\n" );
                return -1;
        }

	memset( &desc, 0, sizeof( struct ual_desc_pci ));

	i = sscanf(device, "%x:%x", &b, &d);
	if (i == 2) {
		b &= 0xFF;
		d &= 0xFF;
		desc.devid = (b << 16) | (d << 8);
	}
	else {
		flasher_error("Bus/device ID must be in bus:dev format.\n");
		return -1;
	}

        desc.data_width = 4;
	desc.bar = 4; /**< PCI Base Address Register to access */
	desc.size = 4096; /**< number of bytes to maps (PAGE_SIZE aligned) */
	desc.offset = 0;
        desc.flags = 0;

	ual = ual_open( UAL_BUS_PCI,  &desc);

        if (!ual) {
                flasher_error("Can't initialize UAL: %s\n",
                              ual_strerror(errno));
                return -1;
        }

        bus = malloc(sizeof(struct zynqus_bus_t));
        bus->bar = ual;
	bus->base = 0;

	zynqus_qspi_init(bus);

        return 0;
}

static void zynqus_flash_help_pcie(struct algo_t *self)
{
	flasher_report("Programs the FPGA Flash of a generic CO-supported ZynqUS board with a binary FPGA image.\n");
	flasher_report("\nUsage: ht-flasher -a %s -d <device_id> -w <filename.bin>\n", self->name);
	flasher_report("The device ID is in bus:dev_fn format (as in lspci). For example, if the device is on PCI bus 0xa at dev/function 0, the string should be '-d 0a:00'.\n");
}


static int zynqus_flash_run(struct algo_t *self)
{
	struct spi_flash_chip_t* chip = &spi_flash_chip_m25qu512;

	if (do_info) {
		for (unsigned i = 0; i < 2; i++) {
			bus->cs = 1 << i;
			printf ("Flash #%u (cs=%u)\n", i, bus->cs);
			zynqus_qspi_dump_reg(bus, "id", 0x9f, 20);
			zynqus_qspi_dump_reg(bus, "status", 0x05, 1);
			zynqus_qspi_dump_reg(bus, "flag", 0x70, 1);
			zynqus_qspi_dump_reg(bus, "nv conf", 0xb5, 2);
			zynqus_qspi_dump_reg(bus, "volatile conf", 0x85, 1);
			zynqus_qspi_dump_reg(bus, "e volatile conf", 0x65, 1);
			zynqus_qspi_dump_reg(bus, "ext addr", 0xc8, 1);
			zynqus_qspi_dump_otp(bus, "OTP");

			zynqus_qspi_set_cs(bus, 1);
			zynqus_qspi_write8(bus, FLASH_RESET_ENA);
			zynqus_qspi_set_cs(bus, 0);

			zynqus_qspi_set_cs(bus, 1);
			zynqus_qspi_write8(bus, FLASH_RESET_MEM);
			zynqus_qspi_set_cs(bus, 0);
		}
		return 0;
	}

	for (unsigned i = 0; i < 2; i++) {
		bus->cs = 1 << i;
		unsigned id = qspi_flash_read_id(chip, bus);
		if (id != chip->id) {
			flasher_error("bad chip id for cs%u (got %06x, expect %006x)\n", i, id, chip->id);
			return -1;
		}
	}

	if (do_write) {
		struct binary_buffer_t *data = read_binary_file( filename );

		if (!data) {
			flasher_error("Can't open binary file '%s'\n",
                                      filename);
			return -1;
		}

		printf("\n");

		return qspi_flash_program(chip, bus, address,
					  data->data, data->size);
	}
	if (do_read) {
		struct binary_buffer_t data;
		unsigned off;
		int res;
		data.size = length ? length : 0x20000;
		data.data = malloc(data.size);

		bus->cs = 3;

		for (off = 0; off < data.size; ) {
			res = zynqus_qspi_read_data(bus, address + off,
						    data.size - off,
						    data.data + off);
			if (res < 0)
				return -1;
			off += res;
		}
		write_binary_file(filename, &data);
		return 0;
	}

	if (do_wotp) {
		/* Write OTP, the string is filename */
		int len = strlen(filename);
		int size = 64;

		if (len + address > size) {
			flasher_error("string+offset is too long for OTP\n");
			return -1;
		}
		size -= address;

		bus->cs = 1;

		zynqus_qspi_address_mode(bus, FLASH_EX4B);

		qspi_flash_write_enable(chip, bus);

		zynqus_qspi_set_cs(bus, 1);
		zynqus_qspi_write8(bus, 0x42); /* OTP Program */
		zynqus_qspi_addr_sz(bus, address, 0);
		zynqus_qspi_set_len(bus, len, (GQSPI_GEN_FIFO_TX
					      | GQSPI_GEN_FIFO_SPI));

		zynqus_qspi_exec(bus);

		zynqus_qspi_program_wait(bus, (uint8_t*)filename, len, len);
	}
	if (0) {
		/* Read Data */

		uint8_t data[0x50];

		bus->cs = 3;

		zynqus_qspi_read_data(bus, address, sizeof (data), data);
		for (unsigned i = 0; i < sizeof (data); i++) {
			if ((i % 16) == 0)
				printf("data[%02d]:", i);
			printf(" %02x", data[i]);
			if ((i % 16) == 15 || i == sizeof(data) - 1)
				printf("\n");
		}
	}
#if 0
	struct spi_flash_chip_t *candidates[] = {
		&spi_flash_chip_s25fl128, &spi_flash_chip_mx25l25635f,
		&spi_flash_chip_n25q256, &spi_flash_chip_m25qu512
	};
	struct spi_flash_chip_t *flash = NULL;
	int i;
	uint32_t id = 0;


	for (i = 0; i < sizeof(candidates) / sizeof(candidates[0]); ++i) {
		candidates[i]->bus = zynqus_spi;

		id = spi_flash_read_id(candidates[i]);

		if (id == candidates[i]->id) {
			flash = candidates[i];
			break;
		}
	}

	if (!flash) {
		flasher_error( "Unknown flash type: %x\n", id );
		return -1;
	}

	flasher_report( "Detected flash type: %s\n", flash->name );

	if (do_write) {
		struct binary_buffer_t *data = read_bitfile( filename );

		if (!data) {
			flasher_error("Can't open bitstream file '%s'\n",
                                      filename);
			return -1;
		}

		printf("\n");
		print_bitfile_info( data );

		return spi_flash_program(flash, 0, data->data, data->size);
	} else if (do_read) {
		struct binary_buffer_t data;
		data.size = 0x20000; // fixme flash.size;
		data.data = malloc(data.size);
		int rv = spi_flash_readback(flash, 0, data.data, data.size);
		(void ) rv;
		write_binary_file(filename, &data);
		return 0;
	}
#endif
	return 0;
}

static int zynqus_flash_release(struct algo_t *self)
{
        return 0;
}

static int zynqus_vme_map(unsigned addr)
{
	struct ual_bar_tkn *ual = bus->bar;

	/* Use window 7 */
	unsigned version = ual_readl(ual, 0x0004);
	if (version == 0x3510f003 || version == 0x3510f005) {
		ual_writel(ual, 0x1003c, addr);
		bus->base = 0x1f000;
	} else if (version == 0x3510f006 || version == 0x3510f007 || version == 0x3510f008 || version == 0x3510f009) {
		ual_writel(ual, 0x221c, addr);
		bus->base = 0xf000;
	} else {
		fprintf(stderr, "incorrect board version (get %08x)\n", version);
		return -1;
	}
	return 0;
}

static int zynqus_flash_init_vme(struct algo_t *self, struct arglist_t *args)
{
        const char *sslot = arg_get_value_str( args, "s", NULL );
        unsigned long slot;
        struct ual_desc_vme desc;
	struct ual_bar_tkn *ual;
	char *end;
	unsigned id;
	unsigned ader1;
	unsigned i;

	if (zynqus_flash_init_common(args) < 0)
		return -1;

        if (!sslot) {
                flasher_error("Expect VME slot (use -s SLOT).\n" );
                return -1;
        }

        slot = strtoul(sslot, &end, 0);
        if (*end != 0) {
                flasher_error("Incorrect slot number.\n");
                return -1;
        }

	/* Read ADER1 from CS/CSR */
        memset(&desc, 0, sizeof(desc));
	desc.data_width = UAL_DATA_WIDTH_32;
	desc.am = 0x2f;
	desc.size = 0x80000;
	desc.offset = slot << 19;
        desc.flags = 0; /* Device is LE */

        ual = ual_open (UAL_BUS_VME, &desc);
	if (ual == NULL) {
		fprintf(stderr, "Could not map CS/CSR of VME slot %lu\n", slot);
		return -1;
	}

	ader1 = 0;
	for (i = 0; i < 4; i++) {
		ader1 <<= 8;
		ader1 |= ual_readb(ual, 0x7ff63 + 0x10 + (i << 2));
	}
	ual_close(ual);

	if (((ader1 >> 2) & 0x3f) != 0x39) {
		fprintf(stderr, "VME slot %lu is not mapped (ader1=0x%08x)\n",
			slot, ader1);
		return -1;
	}

	/* Map using ader1 */
        memset(&desc, 0, sizeof(desc));
	desc.data_width = UAL_DATA_WIDTH_32;
	desc.am = 0x39;
	desc.size = 0x10000;
	desc.offset = ader1 & 0xffffff00;
        desc.flags = 0; /* Device is LE */

        ual = ual_open (UAL_BUS_VME, &desc);
	if (ual == NULL) {
		fprintf(stderr, "Could not map VME space at 0x%08x\n",
			(unsigned)desc.offset);
		return -1;
	}

        bus = malloc(sizeof(struct zynqus_bus_t));
        bus->bar = ual;

	/* Check ID and version */
	id = ual_readl(ual, 0x0000);
	if (id != 0x5752454E) {
		fprintf(stderr, "incorrect board identification (got %08x)\n", id);
		return -1;
	}

	if (zynqus_vme_map (0x00FF0F0000) < 0)
		return -1;

	zynqus_qspi_init(bus);

        return 0;
}

static int zynqus_flash_run_vme(struct algo_t *self)
{
	if (zynqus_flash_run(self) < 0)
		return -1;

	if (do_reset) {
		/* Write to CRL_APB.RESET_CTRL */
		if (zynqus_vme_map (0x00FF5e0000) < 0)
			return -1;
		sys_writel(bus, 0x10, 0x0218);
	}
	return 0;
}


static void zynqus_flash_help_vme(struct algo_t *self)
{
	flasher_report("Programs the FPGA Flash of WREn-VME board.\n");
	flasher_report("\nUsage: ht-flasher -a %s -s <slot> -w <filename.bin>\n", self->name);
}

struct algo_t algo_zynqus_flash = {
	"wren-pcie-flash",
	"QSPI Flash for boot.bin on WREN pcie/pxie board",
	zynqus_flash_init_pcie,
	zynqus_flash_help_pcie,
	zynqus_flash_run,
	zynqus_flash_release
};

struct algo_t algo_wren_vme_flash = {
	"wren-vme-flash",
	"QSPI Flash for boot.bin on WREN VME board",
	zynqus_flash_init_vme,
	zynqus_flash_help_vme,
	zynqus_flash_run_vme,
	zynqus_flash_release
};
