/*
 * Copyright (C) 2013 CERN (www.cern.ch)
 * Author: Tomasz Włostowski <tomasz.wlostowski@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 *
 * SVEC/CTRVD/generic VME flash support
 */

#include <errno.h>
#include <unistd.h>
#include <ual.h>
#include <arpa/inet.h>

#include "ht_flasher.h"
#include "sxldr_regs.h"

#define BOOTLOADER_BASE_CR_CSR 0x70000
#define BOOTLOADER_VERSION 2
#define BOOTLOADER_BITSTREAM_BASE 0x100000
#define BOOTLOADER_SDB_BASE 0x600000

/* SDB filesystem header. Fixed for the time being, the final version should simply use libsdbfs. */
static const uint8_t sdb_header[] = {
	0x53, 0x44, 0x42, 0x2d, 0x00, 0x03, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x00, 0xc0,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xce, 0x42, 0x00, 0x00, 0x5f, 0xec,
	0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x2e, 0x20, 0x20, 0x20,
	0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
	0x20, 0x20, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x2c, 0x13, 0xe9, 0x46, 0x69, 0x6c, 0x65, 0x44, 0x61, 0x74, 0x61,
	0x61, 0x66, 0x70, 0x67, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00,
	0x61, 0x66, 0x70, 0x67, 0x61, 0x2e, 0x62, 0x69, 0x6e, 0x20, 0x20, 0x20,
	0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x01, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x33, 0x93, 0x46, 0x69, 0x6c, 0x65,
	0x44, 0x61, 0x74, 0x61, 0x62, 0x6f, 0x6f, 0x74, 0x00, 0x00, 0x00, 0x01,
	0x00, 0x00, 0x00, 0x00, 0x62, 0x6f, 0x6f, 0x74, 0x6c, 0x64, 0x72, 0x2e,
	0x62, 0x69, 0x6e, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x01
};

struct vme_spi_priv_t {
	struct ual_bar_tkn* bar;
	uint32_t boot_offset;
	uint32_t far_offset;
	int data_width;
};

// access type information
static struct ual_desc_vme vme_desc;

static int vme_spi_init( struct spi_bus_t *bus, struct arglist_t* args )
{
	const char *slot_str = arg_get_value_str( args, "s", NULL );
	const char *base_a24_str = arg_get_value_str( args, "b", NULL );
	const char *access_width_str = arg_get_value_str( args, "n", NULL );
	const char *data_width_str = arg_get_value_str( args, "d", NULL );

	int base_a24 = -1;

	if(!slot_str && !base_a24_str)
	{
		flasher_error( "Expected VME slot number.\n" );
		return -1;
	}

	int slot = -1;
	unsigned int access_width = 32; // default access width is 32 bits
	unsigned int data_width = 0;

	if( slot_str )
	{
		int i = sscanf(slot_str, "%d", &slot);
		if (i != 1)
		{
			flasher_error("Expected slot number.\n");
			return -1;
		}
	}

	if( base_a24_str )
	{
		int i = sscanf(base_a24_str, "0x%x", &base_a24);
		if (i != 1)
		{
			flasher_error("Expected A24 base address.\n");
			return -1;
		}
	}

	if ( access_width_str )
	{
		if ( slot_str )
		{
			flasher_error("Access width is only applicable in A24 mode.\n");
			return -1;
		}

		int i = sscanf(access_width_str, "%u", &access_width);
		if (i != 1 || (access_width != 16 && access_width != 32))
		{
			flasher_error("Access width must be 16 or 32.\n");
			return -1;
		}
	}

	if( data_width_str )
	{
		if ( slot_str )
		{
			flasher_error("Data width is only applicable in A24 mode.\n");
			return -1;
		}

		int i = sscanf(data_width_str, "%u", &data_width);
		if (i != 1 || (data_width != 16 && data_width != 32))
		{
			flasher_error("Data width must be 16 or 32.\n");
			return -1;
		}
	}

	// default data width is same as access width
	if( !data_width )
	{
		data_width = access_width;
	}

	struct vme_spi_priv_t *priv = malloc(sizeof(struct vme_spi_priv_t));

	priv->far_offset = 0;
	priv->boot_offset = 0;
	priv->data_width = data_width;

	if( slot >= 0 )
	{
		vme_desc.data_width = 4;
		vme_desc.am = 0x2f; // CR/CSR
		vme_desc.size = 0x80000;
		vme_desc.flags = UAL_BAR_FLAGS_DEVICE_BE;
		vme_desc.offset = 0x80000 * slot;
		flasher_report("Using CR/CSR mode (slot number: %d)\n", slot);
	}
	else if ( base_a24 >= 0 )
	{
		vme_desc.data_width = access_width / 8;
		vme_desc.am = 0x39; // A24
		vme_desc.size = 0x1000;
		vme_desc.flags = UAL_BAR_FLAGS_DEVICE_BE;

		if( base_a24 & 0xfff )
		{
			priv->boot_offset = base_a24 & 0xfff;
			base_a24 &= 0xfffff000;
		}

		vme_desc.offset = base_a24;

		flasher_report("Using A24D%u mode (data width = %u bits, base address: 0x%x, boot offset 0x%x)\n", access_width, data_width, base_a24, priv->boot_offset );
	}

	struct ual_bar_tkn *ual = ual_open( UAL_BUS_VME, &vme_desc);

	if (!ual)
	{
		flasher_error("Can't initialize UAL: %s\n", ual_strerror(errno));
		return -1;
	}

	priv->bar = ual;
	bus->priv = priv;

	return 0;
}

static int vme_spi_release( struct spi_bus_t *bus )
{
	struct vme_spi_priv_t *priv = bus->priv;

	ual_close( priv->bar );
	free(priv);

	return 0;
}

static uint32_t vme_read(struct spi_bus_t *bus, uint32_t addr)
{
	struct vme_spi_priv_t* priv = (struct vme_spi_priv_t*) bus->priv;
	struct ual_bar_tkn* bar = priv->bar;

	switch (priv->data_width)
	{
		case 16: return ual_readw(bar, addr + priv->boot_offset); break;
		case 32: return ual_readl(bar, addr + priv->boot_offset); break;
		default: fprintf(stderr, "Unhandled data width"); break;
	}

	return -1;
}

static void vme_write( struct spi_bus_t *bus, uint32_t data, uint32_t addr)
{
	struct vme_spi_priv_t* priv = (struct vme_spi_priv_t*) bus->priv;
	struct ual_bar_tkn* bar = ((struct vme_spi_priv_t*)bus->priv)->bar;

	switch (priv->data_width)
	{
		case 16: ual_writew(bar, addr + priv->boot_offset, data); break;
		case 32: ual_writel(bar, addr + priv->boot_offset, data); break;
		default: fprintf(stderr, "Unhandled data width"); break;
	}
}

static void vme_spi_set_cs( struct spi_bus_t *bus, int cs)
{
	struct vme_spi_priv_t* priv = (struct vme_spi_priv_t*) bus->priv;
	vme_write(bus, cs ? SXLDR_FAR_CS : 0, priv->far_offset);
	usleep(1);
}

static uint8_t vme_spi_read8(struct spi_bus_t *bus)
{
	struct vme_spi_priv_t* priv = (struct vme_spi_priv_t*) bus->priv;
	uint32_t far;

	vme_write(bus, SXLDR_FAR_XFER | SXLDR_FAR_DATA_W(0xff) | SXLDR_FAR_CS,
		priv->far_offset);

	do {
		far = vme_read(bus, priv->far_offset);
	} while (!(far & SXLDR_FAR_READY));

	return SXLDR_FAR_DATA_R(far);
}

static void vme_spi_write8(struct spi_bus_t *bus, uint8_t data)
{
	struct vme_spi_priv_t* priv = (struct vme_spi_priv_t*) bus->priv;
	uint32_t far;

	vme_write(bus, SXLDR_FAR_XFER | SXLDR_FAR_DATA_W(data) | SXLDR_FAR_CS,
		priv->far_offset);

	do {
		far = vme_read(bus, priv->far_offset);
	} while (!(far & SXLDR_FAR_READY));
}

static void vme_spi_udelay( struct spi_bus_t *bus, int us )
{
	udelay(us);
}

static struct spi_bus_t spi_bus_generic_flash =
{
	vme_spi_init,
	vme_spi_release,
	vme_spi_set_cs,
	vme_spi_read8,
	vme_spi_write8,
	vme_spi_udelay
};

////////////

static void svec_enter_bootloader( struct spi_bus_t *bus )
{
	int i = 0;
	const uint32_t boot_seq[8] =
		{ 0xde, 0xad, 0xbe, 0xef, 0xca, 0xfe, 0xba, 0xbe };

	int retries = 3;

	while(retries > 0)
	{
		/* magic sequence: unlock bootloader mode, disable application FPGA */
		for (i = 0; i < 8; i++)
			vme_write(bus, boot_seq[i], SXLDR_REG_BTRIGR);

		if (vme_read(bus, SXLDR_REG_IDR) == 0x53564543) 	/* "SVEC" in hex */
			break;

		retries--;
	}

	if( retries == 0 )
	{
		fprintf(stderr,
			"The bootloader is not responding. Are you sure the slot you've\
	specified hosts a SVEC/CTRVD card? Is the SVEC/CTRVD's System FPGA programmer (the \"SFPGA\
	 Done\" LED next to the fuses should be on).\n");
		exit(-1);
	}

	int version = SXLDR_CSR_VERSION_R(vme_read(bus, SXLDR_REG_CSR ));
	printf("Bootloader version: %d\n", version);
}

#if 0
/* Tests the presence of the SPI master in the bootloader to check if we are running
   version > 1 (v1 does not have the version ID register) */
static int spi_test_presence()
{
	vme_write(bus,	SXLDR_FAR_XFER | SXLDR_FAR_DATA_W(0xff),
		 SXLDR_REG_FAR);

	usleep(100000);

	uint32_t far = vme_read(SXLDR_REG_FAR);

	/* transaction is not complete after so much time? no SPI... */
	return (far & SXLDR_FAR_READY);
}
#endif

static int svec_spi_init( struct spi_bus_t *bus, struct arglist_t* args )
{
	int ret = vme_spi_init(bus, args);

	if (ret)
		return ret;

	struct vme_spi_priv_t *priv = bus->priv;

	// adjust bootloader offset depending on the access mode
	switch (vme_desc.am)
	{
		case 0x2f:		// CR/CSR
			priv->boot_offset = 0x70000;
			priv->far_offset = SXLDR_REG_FAR;
			break;

		case 0x39:		// A24
			priv->boot_offset = 0xff00;
			priv->far_offset = SXLDR_REG_FAR;
			break;

		default:
			flasher_error("Unhandled address modifier\n");
			return -1;
	}

	svec_enter_bootloader(bus);

	return 0;
}

static struct spi_bus_t spi_bus_svec_flash =
{
	svec_spi_init,
	vme_spi_release,
	vme_spi_set_cs,
	vme_spi_read8,
	vme_spi_write8,
	vme_spi_udelay
};

////////////

static char *filename;
static struct spi_bus_t* vme_spi;
static int do_write_generic = 0;
static int do_write_app = 0;
static int do_write_boot = 0;
static int do_boot_afpga = 0;

static int vme_flash_init( struct algo_t *algo, struct arglist_t *args )
{
	vme_spi = &spi_bus_generic_flash;
	filename = (char *) arg_get_value_str( args, "w", NULL );

	do_write_generic = arg_is_present( args, "w" );

	if( !filename )
	{
		flasher_error("Filename expected.\n");
		return -1;
	}

	if( vme_spi->init( vme_spi, args ) < 0)
		return -1;

	return 0;
}


static int svec_afpga_flash_init( struct algo_t *algo, struct arglist_t *args )
{
	vme_spi = &spi_bus_svec_flash;
	filename = (char *) arg_get_value_str( args, "w", NULL );

	do_write_app = arg_is_present( args, "w" );

	if( !filename )
	{
		flasher_error("Filename expected.\n");
		return -1;
	}

	if( vme_spi->init( vme_spi, args ) < 0)
		return -1;

	return 0;
}


static int svec_afpga_direct_init( struct algo_t *algo, struct arglist_t *args )
{
	vme_spi = &spi_bus_svec_flash;
	filename = (char *) arg_get_value_str( args, "w", NULL );

	do_boot_afpga = arg_is_present( args, "w" );

	if( !filename )
	{
		flasher_error("Filename expected.\n");
		return -1;
	}

	if( vme_spi->init( vme_spi, args ) < 0)
		return -1;

	return 0;
}


static int svec_sfpga_flash_init( struct algo_t *algo, struct arglist_t *args )
{
	vme_spi = &spi_bus_svec_flash;
	filename = (char *) arg_get_value_str( args, "w", NULL );

	do_write_boot = arg_is_present( args, "w" );

	if( !filename )
	{
		flasher_error("Filename expected.\n");
		return -1;
	}

	if( vme_spi->init( vme_spi, args ) < 0)
		return -1;

	return 0;
}


static int program_afpga( struct spi_bus_t* bus, uint8_t *buf, size_t size)
{
	size_t i = 0;

	vme_write(bus, SXLDR_CSR_SWRST, SXLDR_REG_CSR);
	vme_write(bus, SXLDR_CSR_START | SXLDR_CSR_MSBF, SXLDR_REG_CSR);

	flasher_report("Writing bitstream...\n");

	while(i < size) {
		if(! (vme_read(bus, SXLDR_REG_FIFO_CSR) & SXLDR_FIFO_CSR_FULL))
		{

			uint32_t word = *(uint32_t *) ( buf + i );
			size_t n = (size-i>4?4:size-i);
			vme_write(bus, (n - 1) | ((n<4) ? SXLDR_FIFO_R0_XLAST : 0), SXLDR_REG_FIFO_R0);
			vme_write(bus, htonl(word), SXLDR_REG_FIFO_R1);
			i+=n;
		}
	}

	flasher_report("Waiting for AFPGA to boot up...\n");

	while(1)
	{
		uint32_t rval = vme_read(bus, SXLDR_REG_CSR);

		if(rval & SXLDR_CSR_DONE) {
			flasher_report("Bitstream loaded, status: %s\n", (rval & SXLDR_CSR_ERROR ? "ERROR" : "OK"));
			/* give the VME bus control to App FPGA */
			vme_write(bus, SXLDR_CSR_EXIT, SXLDR_REG_CSR);

			if ( rval & SXLDR_CSR_ERROR )
				return -1;
			else
				return 0;
		}
	}

	return 0;
}


static int vme_flash_run(struct algo_t *self)
{
	struct spi_flash_chip_t *candidates[] = {
		&spi_flash_chip_s25fl128, &spi_flash_chip_mx25l25635f,
		&spi_flash_chip_n25q128, &spi_flash_chip_m25p128,
		&spi_flash_chip_m25qu512, &spi_flash_chip_epcs64
	};

	struct spi_flash_chip_t *flash = NULL;
	int i;

	uint32_t id = spi_flash_read_id(vme_spi);

	for (i = 0; i < sizeof(candidates) / sizeof(candidates[0]); ++i)
	{
		candidates[i]->bus = vme_spi;
		if (id == candidates[i]->id)
		{
			flash = candidates[i];
			break;
		}
	}

	if (!flash)
	{
		flasher_error( "Unknown flash type: %x\n", id );
		return -1;
	}

	flasher_report( "Detected flash type: %s\n", flash->name );

	flash->bus = vme_spi;

	struct binary_buffer_t *data = read_bitfile( filename );

	if(!data)
	{
		flasher_error("Can't open bitstream file '%s'\n", filename);
		return -1;
	}

	printf("\n");
	print_bitfile_info( data );


	if(do_write_app || do_write_boot)
	{
		uint32_t base = do_write_boot ? 0 : BOOTLOADER_BITSTREAM_BASE;

		int ret = spi_flash_program( flash, BOOTLOADER_SDB_BASE, sdb_header, sizeof(sdb_header) );

		if( ret < 0 )
		{
			flasher_error("Failed to program the SDB header");
			return -1;
		}

		return spi_flash_program( flash, base, data->data, data->size );
	}

	else if(do_write_generic)
	{
		return spi_flash_program( flash, 0, data->data, data->size );
	}

	else if(do_boot_afpga)
	{
		flasher_report("Direct AFPGA Boot...\n");
		return program_afpga( vme_spi, data->data, data->size );
	}

	return 0;
}

static int vme_flash_release(struct algo_t *self)
{
	return 0;
}


static void svec_afpga_flash_help( struct algo_t *self )
{
	flasher_report( "Programs the SVEC/CTRVD Application Flash with a binary FPGA bitstream.\n");
	flasher_report( "\nUsage: ht-flasher [-s <slot>] [-b <A24 base>] -w <filename.bin/bit>\n");
}

static void svec_afpga_direct_help( struct algo_t *self )
{
	flasher_report( "Directly programs the SVEC/CTRVD Application FPGA.\n");
	flasher_report( "\nUsage: ht-flasher [-s <slot>] [-b <A24 base>] -w <filename.bin/bit>\n");
}

static void svec_sfpga_flash_help( struct algo_t *self )
{
	flasher_report( "Programs the SVEC/CTRVD Boot Flash with a binary FPGA bitstream.\n");
	flasher_report( "WARNING! PROGRAMMING INCORRECT BITSTREAM WILL BRICK THE BOARD (YOU CAN RECOVER IT ONLY WITH JTAG).\n");
	flasher_report( "\nUsage: ht-flasher [-s <slot>] [-b <A24 base>] -w <filename.bin/bit>\n");
}

static void vme_flash_help( struct algo_t *self )
{
	flasher_report( "Programs a generic VME device flash with a binary FPGA bitstream.\n");
	flasher_report( "\nUsage: ht-flasher [-b <A24 base>] [-n <access width>] -w <filename.bin/bit>\n");
}

static void eda_02175_flash_help( struct algo_t *self )
{
	flasher_report( "Programs the EPCS64 flash memory with a binary FPGA bitstream.\n");
	flasher_report( "\nUsage: ht-flasher -n 16 [-b <A24 address of FAR register>] -w <filename.rpd>\n");
}


struct algo_t algo_svec_app_flash = {
	"svec-app-flash",
	"Program SVEC/CTRVD Application FPGA image",

	svec_afpga_flash_init,
	svec_afpga_flash_help,
	vme_flash_run,
	vme_flash_release
};

struct algo_t algo_svec_app_direct = {
	"svec-app-direct",
	"Direct load of SVEC/CTRVD Application FPGA image",
	svec_afpga_direct_init,
	svec_afpga_direct_help,
	vme_flash_run,
	vme_flash_release
};

struct algo_t algo_svec_boot_flash = {
	"svec-boot-flash",
	"Program SVEC/CTRVD Boot FPGA image (CAN BRICK YOUR BOARD!)",

	svec_sfpga_flash_init,
	svec_sfpga_flash_help,
	vme_flash_run,
	vme_flash_release
};

struct algo_t algo_generic_vme = {
	"generic-vme",
	"Program FPGA image in a VME device",

	vme_flash_init,
	vme_flash_help,
	vme_flash_run,
	vme_flash_release
};

struct algo_t algo_eda_02175 = {
	"eda-02175",
	"Program FPGA image in a EDA-02175-V2 (PS 1 Turn Delay Feedback board)",

	vme_flash_init,
	eda_02175_flash_help,
	vme_flash_run,
	vme_flash_release
};
