
/*
 * Copyright (C) 2013-2018 CERN (www.cern.ch)
 * Author: Tomasz Włostowski <tomasz.wlostowski@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 *
 */

#include "ht_flasher.h"

#include <sys/errno.h>
#include <string.h>
#include <ual.h>

#define EEPROM_ADDR 0x56

struct spec_i2c_priv_t
{
	struct ual_bar_tkn *bar4;
};

static uint32_t gennum_readl(struct i2c_bus_t *bus, uint32_t addr)
{
	struct ual_bar_tkn *bar = ((struct spec_i2c_priv_t *)bus->priv)->bar4;
	uint32_t d = ual_readl(bar, addr);
	//printf("readl 0x%x %x\n", addr, d );
	return d;
}

static void gennum_writel(struct i2c_bus_t *bus, uint32_t data, uint32_t addr)
{
	struct ual_bar_tkn *bar = ((struct spec_i2c_priv_t *)bus->priv)->bar4;
	ual_writel(bar, addr, data);
}

static int spec_i2c_init(struct i2c_bus_t *bus, struct arglist_t *args)
{
	const char *device = arg_get_value_str(args, "d", NULL);

	if (!device)
	{
		flasher_error("Expected PCI bus/device ID.\n");
		return -1;
	}

	struct ual_desc_pci desc;

	int b, d;
	int i = sscanf(device, "%x:%x", &b, &d);
	if (i == 2)
	{
		b &= 0xFF;
		d &= 0xFF;
		desc.devid = (b << 16) | (d << 8);
	}
	else
	{
		flasher_error("Bus/device ID must be in bus:dev format.\n");
		return -1;
	}

	desc.data_width = 4;
	desc.bar = 4;		/**< PCI Base Address Register to access */
	desc.size = 0x1000; /**< number of bytes to maps (PAGE_SIZE aligned) */
	desc.offset = 0;
	desc.flags = 0;

	struct ual_bar_tkn *ual = ual_open(UAL_BUS_PCI, &desc);

	if (!ual)
	{
		flasher_error("Can't initialize UAL: %s\n", ual_strerror(errno));
		return -1;
	}

	struct spec_i2c_priv_t *priv = malloc(sizeof(struct spec_i2c_priv_t));
	priv->bar4 = ual;
	bus->priv = priv;

	return 0;
}

static void spec_i2c_release(struct i2c_bus_t *bus)
{
	struct spec_i2c_priv_t *priv = bus->priv;

	ual_close(priv->bar4);
	free(priv);

	//return 0;
}

#define LB_CTL 0x804
#define TWI_CTRL 0x900
#define TWI_STATUS 0x904
#define TWI_IRT_STATUS 0x910
#define I2C_ADDR 0x908
#define I2C_DATA 0x90C
#define TWI_TR_SIZE 0x914

int spec_i2c_read(struct i2c_bus_t *bus, uint8_t addr, uint8_t *data, int size)
{

	/* Shut off EEPROM_INIT state machine if not done so */
	uint32_t tmp = gennum_readl(bus, LB_CTL);
	if ((tmp & 0x10000) == 0)
	{
		tmp |= 0x10000;
		gennum_writel(bus, tmp, LB_CTL);
	}


	// Init I2C clock Fpci/(22*Fscl)=(DIV_A+1)*(DIV_B+1)
	// CLR_FIFO=1, SLVMON=0, HOLD=0, ACKEN=1, NEA=1, MS=1, RW=0
   	gennum_writel(bus, 0x384F, TWI_CTRL);

	tmp = gennum_readl(bus, TWI_CTRL);
	(void)tmp;
	tmp = gennum_readl(bus, TWI_IRT_STATUS);
	(void)tmp;

	// Wait until I2C bus is idle
	int i = 2000000;
	while (i > 0)
	{
		i--;
		tmp = gennum_readl(bus, TWI_STATUS);
		//printf("wait-busy-read stat 0x%08x\n", tmp);

		if ((tmp & 0x100) == 0)
		{
		//	printf("wait-busy-read complete @ %d\n", i);
			break;
		}


		udelay(100);
	}


	// Perform sequential page read from the start address
	int error_flag = 0;
	int total_transfer = 0;
	while (size > 0 && error_flag == 0)
	{
		int transfer_len;
		// Transfer bigger than I2C fifo (8 bytes) are split
		if (size > 8)
		{
			transfer_len = 8;
			size -= 8;
		}
		else
		{
			transfer_len = size;
			size = 0;
		}

		//printf("Expect %d read bytes\n", transfer_len );
		// Update expected receive data size
		gennum_writel(bus, transfer_len, 0x914);
		// Write device address
		gennum_writel(bus, 0x7f & addr, I2C_ADDR);
		// Wait until transfer is completed
		int i = 1000000;
		while (i > 0)
		{
			i--;
			tmp = gennum_readl(bus, TWI_IRT_STATUS);
			uint32_t st = gennum_readl(bus, TWI_STATUS);
			uint32_t trs = gennum_readl(bus, TWI_TR_SIZE);
                        (void)st;
                        (void)trs;
			//printf("read irt status 0x%08x st 0x%08x trs %d\n", tmp, st, trs);
			if (tmp & 0x1)
			{
			//	printf("xfer complete @ %d\n", i);
				break;
			}
		}

		if (i == 0)
		{
			return -1;
		}

		// Read data from fifo
		while (transfer_len > 0)
		{
			data[total_transfer] = gennum_readl(bus, I2C_DATA) & 0xff;
			transfer_len -= 1;
			total_transfer += 1;
		}
	}
	return 0;
}


int spec_i2c_write(struct i2c_bus_t *bus, uint8_t addr, uint8_t *data, int size)
{
	//printf("[spec i2c write %d bytes]\n", size);

	uint32_t tmp = gennum_readl(bus, LB_CTL);
	if ((tmp & 0x10000) == 0)
	{
		tmp |= 0x10000;
		gennum_writel(bus, tmp, LB_CTL);
	}

	// Read to clear TWI_IRT_STATUS
	tmp = gennum_readl(bus, TWI_IRT_STATUS);
	(void) tmp;
	// Read to clear TWI_STATUS
	tmp = gennum_readl(bus, TWI_STATUS);
	(void) tmp;
	// Init I2C clock Fpci/(22*Fscl)=(DIV_A+1)*(DIV_B+1)
	// CLR_FIFO=1, SLVMON=0, HOLD=1, ACKEN=1, NEA=1, MS=1, RW=0
	gennum_writel(bus, 0x384E /*| (1<<4)*/, TWI_CTRL);
	// Read back from register to guarantee the mode change
	tmp = gennum_readl(bus, TWI_CTRL);
	(void)tmp;

	// Read to clear TWI_IRT_STATUS
	tmp = gennum_readl(bus, TWI_IRT_STATUS);
	(void) tmp;


	// Wait until I2C bus is idle
	int i = 1000000;
	while (i > 0)
	{
		i--;
		tmp = gennum_readl(bus, TWI_STATUS);
		//printf("wait-busy-write stat 0x%08x\n", tmp);

		if ((tmp & 0x100) == 0)
		{
		//	printf("busy-write complete @ %d\n", i);
			break;
		}

		udelay(10);
	}

	// Perform sequential page write from the start address
	int error_flag = 0;
	int total_transfer = 0;
	while (size > 0 && error_flag == 0)
	{
		int transfer_len;
		// Transfer bigger than I2C fifo (8 bytes) are split
		if (size > 8)
		{
			transfer_len = 8;
			size -= 8;
		}
		else
		{
			transfer_len = size;
			size = 0;
		}

		//printf("Write %d bytes [s %d]\n", transfer_len, size);

		tmp = gennum_readl(bus, TWI_IRT_STATUS);
		(void) tmp;

		// Read data from fifo
		while (transfer_len > 0)
		{
			gennum_writel(bus, data[total_transfer] & 0xff, I2C_DATA);
			transfer_len -= 1;
			total_transfer += 1;
		}



		// Write device address
		gennum_writel(bus, 0x7f & addr, I2C_ADDR);



		// Wait until transfer is completed
		int i = 100000;
		while (i > 0)
		{
			i--;
			tmp = gennum_readl(bus, TWI_IRT_STATUS);
			//printf("wait-write-done irt_stat 0x%08x\n", tmp );
			if (tmp & 0x1)
			{
				int stat = gennum_readl(bus, 0x914);
			//	printf("write-done @ %d %d\n", i, stat);
                                (void)stat;
				break;
			}

			if (tmp & 0xc)
			{
			//	printf("write-fail @ %d\n", i);
				break;
			}
			udelay(10);
		}

		if (i == 0)
		{
			//printf("write-done-timeout\n");
			return -1;
		}
	}

	return 0;
}

static struct i2c_bus_t spec_bus_i2c_eeprom = {
	spec_i2c_init,
	spec_i2c_release,
	spec_i2c_read,
	spec_i2c_write};

static char *filename;
static struct i2c_bus_t *spec_i2c = &spec_bus_i2c_eeprom;
static int do_read = 0;
static int do_write = 0;

static int spec_eeprom_init(struct algo_t *self, struct arglist_t *args)
{
	if (spec_i2c->init(spec_i2c, args) < 0)
		return -1;

	do_read = arg_is_present(args, "r");
	do_write = arg_is_present(args, "w");

	if (do_read)
	{
		filename = (char *)arg_get_value_str(args, "r", NULL);
	}

	if (do_write)
	{
		filename = (char *)arg_get_value_str(args, "w", NULL);
	}

	return 0;
}

static void spec_eeprom_help(struct algo_t *self)
{
	flasher_report("Programs SPEC GN4124 configuration EEPROM with a binary file.\n");
	flasher_report("\nUsage: ht-flasher [-r] <filename.bin>\n");
}

static int max(int a, int b)
{
	return a > b ? a : b;
}

static int spec_eeprom_run(struct algo_t *self)
{
	uint8_t buf[129], buf_rdbk[129];

	if (do_read)
	{
		int count = 128;

		buf[0] = 0;

		if (spec_i2c->write(spec_i2c, EEPROM_ADDR, buf, 1) < 0)
		{
			flasher_error("Reading EEPROM failed.\n");
			return -1;
		}

		if (spec_i2c->read(spec_i2c, EEPROM_ADDR, buf, count) < 0)
		{
			flasher_error("Reading EEPROM failed.\n");
			return -1;
		}

		if (!filename)
		{
			flasher_error("File name expected.\n");
			return -1;
		}

		FILE *f = fopen(filename, "wb");

		fwrite(buf, 1, count, f);
		fclose(f);

		flasher_report("%d bytes read from EEPROM and saved to %s.\n", count, filename);
		return 0;
	}
	else if (do_write)
	{
		struct binary_buffer_t *data = read_binary_file(filename);
		if (!data)
		{
			flasher_error("Can't read %s\n.", filename);
			return 1;
		}

		flasher_report("Writing %d bytes read from %s to the EEPROM.\n", data->size, filename);

		int i;
		for (i = 0; i < data->size; i++)
		{
			buf[0] = i;
			buf[1] = data->data[i];

			if( spec_i2c->write(spec_i2c, EEPROM_ADDR, buf, 2) < 0)
			{
				flasher_error("Flashing failed (I2C error)\n");
			}

			udelay(6000);
		}

		buf_rdbk[0] = 0;

		if (spec_i2c->write(spec_i2c, EEPROM_ADDR, buf_rdbk, 1) < 0)
		{
			flasher_error("Readback failed.\n");
			return -1;
		}

		if (spec_i2c->read(spec_i2c, EEPROM_ADDR, buf_rdbk, 128) < 0)
		{
			flasher_error("Readback failed.\n");
			return -1;
		}

		if (memcmp(buf_rdbk, data->data, max(data->size, 128)))
		{
			flasher_error("Verification failed.\n");
			return -1;
		}

		flasher_report("Verification OK.\n");
	}
	else
	{

		flasher_error("No operation specified. We support [-r]ead and [-w]rite.\n");
		return -1;
	}

	return 0;
}

static int spec_eeprom_release(struct algo_t *self)
{
	spec_i2c->release(spec_i2c);
	return 0;
}

struct algo_t algo_spec_eeprom = {
	"spec-eeprom",
	"I2C EEPROM for GN4124 configuration on the SPEC carrier",

	spec_eeprom_init,
	spec_eeprom_help,
	spec_eeprom_run,
	spec_eeprom_release};
