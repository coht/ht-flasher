/*
 * Copyright (C) 2020 CERN (www.cern.ch)
 *
 * Released according to the GNU GPL, version 2 or any later version.
 *
 */

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/errno.h>
#include "ual.h"

#include "ht_flasher.h"

/* FAR register.  From wr2rf files.  */
#define WR2RF_FAR (0x100)

#define WR2RF_FAR_DATA_MASK 0xff
#define WR2RF_FAR_XFER 0x00000100
#define WR2RF_FAR_READY 0x00000200
#define WR2RF_FAR_CS 0x00000400


static void wr2rf_spi_set_cs(struct spi_bus_t *bus, int cs)
{
        struct ual_bar_tkn *dev = (struct ual_bar_tkn*)bus->priv;

	ual_writew(dev, WR2RF_FAR, cs ? WR2RF_FAR_CS : 0);
}

static uint8_t wr2rf_spi_read8(struct spi_bus_t *bus)
{
        struct ual_bar_tkn *dev = (struct ual_bar_tkn*)bus->priv;
	uint32_t far;

	ual_writew(dev, WR2RF_FAR, WR2RF_FAR_XFER | 0xff | WR2RF_FAR_CS);
	do {
                far = ual_readw(dev, WR2RF_FAR);
	} while (!(far & WR2RF_FAR_READY));

	return far & WR2RF_FAR_DATA_MASK;
}

void wr2rf_spi_write8(struct spi_bus_t *bus, uint8_t data)
{
        struct ual_bar_tkn *dev = (struct ual_bar_tkn*)bus->priv;
	uint32_t far;

	ual_writew(dev, WR2RF_FAR, WR2RF_FAR_XFER | (uint32_t) data | WR2RF_FAR_CS);
	do {
		far = ual_readw(dev, WR2RF_FAR);
	} while (!(far & WR2RF_FAR_READY));
}

static int wr2rf_spi_init( struct spi_bus_t *bus, struct arglist_t* args )
{
        const char *sslot = arg_get_value_str( args, "s", NULL );
        unsigned long slot;
        char *end;
        struct ual_desc_vme desc;

        if(!sslot)
        {
                flasher_error("Expect VME slot (use -s SLOT).\n" );
                return -1;
        }

        slot = strtoul(sslot, &end, 0);
        if (*end != 0)
        {
                flasher_error("Incorrect slot number.\n");
                return -1;
        }


        memset(&desc, 0, sizeof(desc));
	desc.data_width = UAL_DATA_WIDTH_32;
	desc.am = 0x39;
	desc.size = 0x8000;
	desc.offset = slot << 19;
        desc.flags = UAL_BAR_FLAGS_DEVICE_BE;

        struct ual_bar_tkn *ual = ual_open (UAL_BUS_VME, &desc);

	if (ual == NULL) {
		fprintf(stderr, "Could not map VME space at 0x%08x\n",
			(unsigned)desc.offset);
		exit(1);
	}

        bus->priv = ual;

	// make sure the SPI bus is in a known state
	wr2rf_spi_set_cs(bus, 1);
	volatile uint8_t tmp = wr2rf_spi_read8(bus);
	wr2rf_spi_set_cs(bus, 0);
        (void)tmp;

        return 0;
}

static int wr2rf_spi_release( struct spi_bus_t *bus )
{
        struct ual_bar_tkn *dev = (struct ual_bar_tkn*)bus->priv;
	ual_close(dev);

        return 0;
}

static void wr2rf_spi_udelay( struct spi_bus_t *bus, int us )
{
    udelay(us);
}

struct spi_bus_t spi_bus_wr2rf_flash =
{
    wr2rf_spi_init,
    wr2rf_spi_release,
    wr2rf_spi_set_cs,
    wr2rf_spi_read8,
    wr2rf_spi_write8,
    wr2rf_spi_udelay
};

static char *filename;
static struct spi_bus_t* wr2rf_spi = &spi_bus_wr2rf_flash;
static int do_read, do_write;

static int wr2rf_flash_init( struct algo_t *self, struct arglist_t *args )
{
	printf("FAR @ 0x%08x\n", WR2RF_FAR);
	do_read = arg_is_present(args, "r");
	do_write = arg_is_present(args, "w");

	if (do_read)
	{
		filename = (char *)arg_get_value_str(args, "r", NULL);
	}

	if (do_write)
	{
		filename = (char *)arg_get_value_str(args, "w", NULL);
	}


	if( wr2rf_spi->init( wr2rf_spi, args ) < 0)
		return -1;

	return 0;
}

static void wr2rf_flash_help( struct algo_t *self )
{
	flasher_report( "Programs the FPGA Flash of thw wr2rf-vme device with a binary FPGA bitstream.\n");
	flasher_report( "\nUsage: ht-flasher -a -s <slot> -w <filename.bin>\n");
}


static int wr2rf_flash_run(struct algo_t *self)
{
	struct spi_flash_chip_t *flash = &spi_flash_chip_s25fl128l;

	uint32_t id = spi_flash_read_id(wr2rf_spi);

	if (id != flash->id)
	{
		flasher_error( "Unknown flash type: %08x\n", id );
		return -1;
	}

        flash->bus = wr2rf_spi;
	flasher_report( "Detected flash type: %s\n", flash->name );

	if( do_write )
	{
		struct binary_buffer_t *data = read_bitfile( filename );

		if(!data)
		{
			flasher_error("Can't open bitstream file '%s'\n", filename);
			return -1;
		}

		printf("\n");
		print_bitfile_info( data );

		return spi_flash_program( flash, 0, data->data, data->size );
	}
	else if (do_read) {
		struct binary_buffer_t data;
		data.size = 0x20000; // fixme flash.size;
		data.data = malloc(data.size);
		int rv = spi_flash_readback(flash, 0, data.data, data.size);
		(void ) rv;
		write_binary_file(filename, &data);
		return 0;
	}
	else
		flasher_error("no command executed");
        return 0;
}

static int wr2rf_flash_release(struct algo_t *self)
{
        return 0;
}

struct algo_t algo_wr2rf_flash = {
	"wr2rf-flash",
	"SPI Flash for bitstream storage on the wr2rf-vme board",

	wr2rf_flash_init,
	wr2rf_flash_help,
	wr2rf_flash_run,
	wr2rf_flash_release
};
