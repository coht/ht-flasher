
/*
 * This work is part of the White Rabbit project
 *
 * Copyright (C) 2011-2018 CERN (www.cern.ch)
 * Author: Tomasz Wlostowski <tomasz.wlostowski@cern.ch>
 * Author: Grzegorz Daniluk <grzegorz.daniluk@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */

#include "ht_flasher.h"



static void mi2c_start(struct i2c_bus_t *bus)
{
    struct i2c_bitbang_t *bb = (struct i2c_bitbang_t*) bus->priv;
	
    bb->set_sda(bb, 0);
    bb->set_scl(bb, 0);
}


static void mi2c_repeat_start(struct i2c_bus_t *bus)
{
    struct i2c_bitbang_t *bb = (struct i2c_bitbang_t*) bus->priv;
	
    bb->set_sda(bb, 1);
    bb->set_scl(bb, 1);
    bb->set_sda(bb, 0);
    bb->set_scl(bb, 0);
}

static void mi2c_stop(struct i2c_bus_t *bus)
{
    struct i2c_bitbang_t *bb = (struct i2c_bitbang_t*) bus->priv;

    bb->set_sda(bb, 0);
    bb->set_scl(bb, 1);
    bb->set_sda(bb, 1);
}

static int mi2c_write8(struct i2c_bus_t *bus, uint8_t data)
{
    struct i2c_bitbang_t *bb = (struct i2c_bitbang_t*) bus->priv;
    int i, ack;
	
	for (i = 0; i < 8; i++, data <<= 1) {
		bb->set_sda(bb, data & 0x80);
		bb->set_scl(bb, 1);
		bb->set_scl(bb, 0);
	}

	bb->set_sda(bb, 1);
	bb->set_scl(bb, 1);

	ack = bb->get_sda(bb);

	bb->set_scl(bb, 0);
	bb->set_sda(bb, 0);

	return ack != 0;
}

static int mi2c_read8(struct i2c_bus_t *bus, uint8_t *data, int last)
{
    struct i2c_bitbang_t *bb = (struct i2c_bitbang_t*) bus->priv;
    
	int i;
	unsigned char indata = 0;

	bb->set_sda(bb, 1);
	/* assert: scl is low */
	bb->set_scl(bb, 0);

	for (i = 0; i < 8; i++) {
		bb->set_scl(bb, 1);
		indata <<= 1;
		if (bb->get_sda(bb))
			indata |= 0x01;
		bb->set_scl(bb, 0);
	}

	if (last) {
		bb->set_sda(bb, 1);	//noack
		bb->set_scl(bb, 1);
		bb->set_scl(bb, 0);
	} else {
		bb->set_sda(bb, 0);	//ack
		bb->set_scl(bb, 1);
		bb->set_scl(bb, 0);
	}

	*data = indata;

    return 0;
}

static int spec_mi2c_init(struct i2c_bus_t *bus, struct arglist_t *args)
{
    struct i2c_bitbang_t *bb = malloc(sizeof(struct i2c_bitbang_t ) );
    
    bus->priv = bb;

    bb->set_scl(bb, 1);
	bb->set_sda(bb, 1);
    
    mi2c_scan(bus);

    return 0;

}

static int spec_mi2c_release(struct i2c_bus_t *bus)
{
    return 0;
}

void mi2c_scan(struct i2c_bus_t *bus)
{
    int i;

    for(i=0;i<0x80;i++)
    {
        mi2c_start(bus);
        if(!mi2c_write8(bus, i<<1)) printf("found : %x\n", i);
        mi2c_stop(bus);
    }
}

struct i2c_bus_t spec_bus_i2c_eeprom =
{
    spec_mi2c_init, 
    spec_mi2c_release,
    mi2c_start,
    mi2c_repeat_start,
    mi2c_stop,
    mi2c_read8,
    mi2c_write8
};
