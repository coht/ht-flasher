
/*
 * Copyright (C) 2013-2018 CERN (www.cern.ch)
 * Author: Tomasz Włostowski <tomasz.wlostowski@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 *
 */

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/errno.h>

#include <ual.h>

#include "ht_flasher.h"

#define GENMTCA_FAR_DATA_MASK 0xff
#define GENMTCA_FAR_XFER 0x00000100
#define GENMTCA_FAR_READY 0x00000200
#define GENMTCA_FAR_CS 0x00000400

struct board_info {
	uint32_t far_register_offset;
	uint32_t map_size;
	uint16_t vendor;
	uint16_t device;
	int is_be;
	const char *name;
};

static struct board_info supported_boards[] =
{
	{ 0x4068,  0x10000,  0x10dc, 0x1af, 0, "sis83k-flash"},
	{ 0x84068, 0x100000, 0x10dc, 0x1b0, 0, "afcz-flash" },
	{ 0x30000, 0x40000,  0x10dc, 0x434, 1, "ctra-flash" },
	{ 0, 0, 0, 0, 0, NULL }
};


static struct board_info *the_board = NULL;
static uint32_t far_register_offset;

struct genmtca_spi_priv_t {
        struct ual_bar_tkn* bar0;
};

static uint32_t sys_readl(struct spi_bus_t *bus, uint32_t addr)
{
        struct ual_bar_tkn* bar = ((struct genmtca_spi_priv_t*)bus->priv)->bar0;
        uint32_t d = ual_readl( bar,  addr );
        return d;
}

static void sys_writel(struct spi_bus_t *bus, uint32_t data, uint32_t addr)
{
        struct ual_bar_tkn* bar = ((struct genmtca_spi_priv_t*)bus->priv)->bar0;
        ual_writel( bar, addr, data );
}

static void genmtca_spi_set_cs(struct spi_bus_t *bus, int cs)
{
	sys_writel(bus, (cs) ? GENMTCA_FAR_CS : 0, far_register_offset);
}

static uint8_t genmtca_spi_read8(struct spi_bus_t *bus)
{
	uint32_t far;
	sys_writel(bus, GENMTCA_FAR_XFER | 0xff | GENMTCA_FAR_CS,
		   far_register_offset);
	do {
		far = sys_readl(bus, far_register_offset);
	} while (!(far & GENMTCA_FAR_READY));

	return far & GENMTCA_FAR_DATA_MASK;
}

void genmtca_spi_write8(struct spi_bus_t *bus, uint8_t data)
{
	uint32_t far;

	sys_writel(bus, GENMTCA_FAR_XFER | (uint32_t) data | GENMTCA_FAR_CS,
                   far_register_offset);
	do {
		far = sys_readl(bus, far_register_offset);
	} while (!(far & GENMTCA_FAR_READY));
}

static int genmtca_spi_init( struct spi_bus_t *bus, struct arglist_t* args )
{
        const char *device = arg_get_value_str(args, "d", NULL);

        if (!device) {
                flasher_error( "Expected PCI bus/device ID.\n" );
                return -1;
        }

        struct ual_desc_pci desc;

	memset( &desc, 0, sizeof( struct ual_desc_pci ));

	int b, d;
	int i = sscanf(device, "%x:%x", &b, &d);
	if (i == 2) {
		b &= 0xFF;
		d &= 0xFF;
		desc.devid = (b << 16) | (d << 8);
	}
	else {
		flasher_error("Bus/device ID must be in bus:dev format.\n");
		return -1;
	}

        desc.data_width = 4;
	desc.bar = 0; /**< PCI Base Address Register to access */
	desc.size = the_board->map_size; /**< number of bytes to maps (PAGE_SIZE aligned) */
	desc.offset = 0;
        desc.flags = the_board->is_be ? UAL_BAR_FLAGS_DEVICE_BE : 0;


	struct ual_bar_tkn *ual = ual_open( UAL_BUS_PCI,  &desc);

        if (!ual) {
                flasher_error("Can't initialize UAL: %s\n",
                              ual_strerror(errno));
                return -1;
        }

        struct genmtca_spi_priv_t *priv;
        priv = malloc(sizeof(struct genmtca_spi_priv_t));
        priv->bar0 = ual;
        bus->priv = priv;

	// make sure the SPI bus is in a known state
	genmtca_spi_set_cs(bus, 1);
	uint8_t tmp = genmtca_spi_read8(bus);
	genmtca_spi_set_cs(bus, 0);
	(void) tmp;

        return 0;
}

static int genmtca_spi_release( struct spi_bus_t *bus )
{
        struct genmtca_spi_priv_t *priv = bus->priv;

        ual_close( priv->bar0 );
        free(priv);

        return 0;
}

static void genmtca_spi_udelay(struct spi_bus_t *bus, int us)
{
        udelay(us);
}

struct spi_bus_t spi_bus_genmtca_flash =
{
    genmtca_spi_init,
    genmtca_spi_release,
    genmtca_spi_set_cs,
    genmtca_spi_read8,
    genmtca_spi_write8,
    genmtca_spi_udelay
};

static char *filename;
static struct spi_bus_t* genmtca_spi = &spi_bus_genmtca_flash;
static int do_read, do_write, do_erase;

static int genmtca_flash_init( struct algo_t *self, struct arglist_t *args )
{
	int i;
	for(i = 0; supported_boards[i].name; i++)
		if(!strcasecmp(supported_boards[i].name, self->name )) {
			printf("Board: %s\n", self->name);
			the_board = &supported_boards[i];
			break;
		}


	far_register_offset = the_board->far_register_offset;
	do_read = arg_is_present(args, "r");
	do_write = arg_is_present(args, "w");
	do_erase = arg_is_present(args, "e");

	if (arg_is_present(args, "f")) {
		const char *tmp = arg_get_value_str(args, "f", NULL);
		if (sscanf(tmp,"%i", &far_register_offset) != 1) {
			flasher_error("I don't understand the format of the FAR register offset.\n");
			return -1;
		}
	}


	if (do_read)
		filename = (char *)arg_get_value_str(args, "r", NULL);

	if (do_write)
		filename = (char *)arg_get_value_str(args, "w", NULL);

	if (genmtca_spi->init(genmtca_spi, args) < 0)
		return -1;

	return 0;
}

static void genmtca_flash_help(struct algo_t *self)
{
	flasher_report("Programs the FPGA Flash of a generic CO-supported MTCA.4 device with a binary FPGA bitstream.\n");
	flasher_report("\nUsage: ht-flasher -a -d <device_id> -w <filename.bin> [-f far_register_offset]\n");
	flasher_report("The device ID is in bus:dev_fn format (as in lspci). For example, if the device is on PCI bus 0xa at dev/function 0, the string should be '-d 0a:00'.\n");
	flasher_report("The optional -f switch allows to set custom address for the Flash Access Register.\n");
}


static int genmtca_flash_run(struct algo_t *self)
{
	struct spi_flash_chip_t *candidates[] = {
		&spi_flash_chip_s25fl128, &spi_flash_chip_mx25l25635f,
		&spi_flash_chip_n25q256, &spi_flash_chip_m25qu512
	};
	struct spi_flash_chip_t *flash = NULL;
	int i;
	uint32_t id = spi_flash_read_id(genmtca_spi);

	for (i = 0; i < sizeof(candidates) / sizeof(candidates[0]); ++i) {
		if (id == candidates[i]->id) {
			flash = candidates[i];
			break;
		}
	}

	if (!flash) {
		flasher_error( "Unknown flash type: %x\n", id );
		return -1;
	}

        flash->bus = genmtca_spi;

	flasher_report( "Detected flash type: %s\n", flash->name );

	if (do_write) {
		struct binary_buffer_t *data = read_bitfile( filename );

		if (!data) {
			flasher_error("Can't open bitstream file '%s'\n",
                                      filename);
			return -1;
		}

		printf("\n");
		print_bitfile_info( data );

		return spi_flash_program(flash, 0, data->data, data->size);
	} else if (do_read) {
		struct binary_buffer_t data;
		data.size = 0x20000; // fixme flash.size;
		data.data = malloc(data.size);
		int rv = spi_flash_readback(flash, 0, data.data, data.size);
		(void ) rv;
		write_binary_file(filename, &data);
		return 0;
	}
	else if ( do_erase )
	{
		flasher_error("Wait for erase flash memory... \n");
		spi_flash_erase_bulk(flash);
		return 0;
	}

	return 0;
}

static int genmtca_flash_release(struct algo_t *self)
{
        return 0;
}

struct algo_t algo_sis83k_flash = {
	"sis83k-flash",
	"SPI Flash for bitstream storage on the SIS8300KU AMC board",

	genmtca_flash_init,
	genmtca_flash_help,
	genmtca_flash_run,
	genmtca_flash_release
};


struct algo_t algo_afcz_flash = {
	"afcz-flash",
	"SPI Flash for bitstream storage on the AFCZ AMC board",

	genmtca_flash_init,
	genmtca_flash_help,
	genmtca_flash_run,
	genmtca_flash_release
};

struct algo_t algo_ctra_flash = {
	"ctra-flash",
	"Flash bitstream for CTR-AMC board (AFC 3.1 carrier + CTR FMC)",

	genmtca_flash_init,
	genmtca_flash_help,
	genmtca_flash_run,
	genmtca_flash_release
};
