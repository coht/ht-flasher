VMEBRIDGE=/acc/local/L867/drv/vmebus/1.0.1

-include Makefile.specific

all:
	make -C ual CONFIG_VME=y VMEBRIDGE=$(VMEBRIDGE)
	make -C src


clean:
	make -C ual clean
	make -C src clean
